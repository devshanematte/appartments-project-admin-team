import React, {
	useState,
	useEffect,
	useMemo
} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { api } from '../../Services';
import { Add } from '@material-ui/icons';

const RightSide = () => {

	const dispatch = useDispatch();

	return useMemo(()=>(
		<div className="right-content-side">
			
			<section>
				<Link to="/apartments">Жилые комплексы</Link>
				<span>
					<Link title="Добавить" to="/apartments/create">
						<Add className="add-icon"/>
					</Link>
				</span>
			</section>

			<section>
				<Link to="/apartments/flats">Квартиры</Link>
			</section>

			<section>
				<Link to="/cities">Города</Link>
				<span>
					<Link title="Добавить" to="/cities/create">
						<Add className="add-icon"/>
					</Link>
				</span>
			</section>

			<section>
				<Link to="/builders">Застройщики</Link>
				<span>
					<Link title="Добавить" to="/builders/create">
						<Add className="add-icon"/>
					</Link>
				</span>
			</section>

			<section>
				<Link to="/users">Пользователи</Link>
			</section>

			<section>
				<Link to="/pages">Страницы</Link>
				<span>
					<Link title="Добавить" to="/pages/create">
						<Add className="add-icon"/>
					</Link>
				</span>
			</section>

			<section>
				<Link to="/settings">Настройки</Link>
			</section>

			<section>
				<span onClick={()=>{dispatch(api.auth.logout())}} className="logout">Выход</span>
			</section>

		</div>
	), [])
}

export default RightSide;