import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

const Blocks = () => {

	return (
		<div className="skeleton-block">
			<div className="skeleton-block-item" style={{width:'100%'}}>
				<Skeleton variant="rect" width="100%" height={370} animation="wave" />
				<Skeleton width="85%" animation="wave" />
				<Skeleton width="85%" animation="wave" />
				<Skeleton width="85%" animation="wave" />
				<Skeleton width="70%" animation="wave" />
				<Skeleton width="70%" animation="wave" />
				<Skeleton width="70%" animation="wave" />
				<Skeleton width="70%" animation="wave" />
			</div>
		</div>
	)
}

export default Blocks;