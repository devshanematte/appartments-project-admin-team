import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

const Blocks = () => {

	return (
		<div className="skeleton-block">
			<div className="skeleton-block-item">
				<Skeleton variant="rect" width="100%" height={260} animation="wave"  />
				<Skeleton width="95%" animation="wave" />
				<Skeleton width="68%" animation="wave" />
			</div>
			<div className="skeleton-block-item">
				<Skeleton variant="rect" width="100%" height={260} animation="wave"  />
				<Skeleton width="70%" animation="wave" />
				<Skeleton width="68%" animation="wave" />
			</div>
			<div className="skeleton-block-item">
				<Skeleton variant="rect" width="100%" height={260} animation="wave"  />
				<Skeleton width="90%" animation="wave" />
				<Skeleton width="68%" animation="wave" />
			</div>
		</div>
	)
}

export default Blocks;