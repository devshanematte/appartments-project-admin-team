import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

const List = () => {

	return (
		<div >
			<div style={{marginBottom:25}}>
				<Skeleton variant="rect" width="100%" height={100} animation="wave"  />
				<Skeleton width="95%" height={33} animation="wave" />
				<Skeleton width="68%" height={22} animation="wave" />
			</div>
			<div style={{marginBottom:25}}>
				<Skeleton variant="rect" width="100%" height={100} animation="wave"  />
				<Skeleton width="95%" height={33} animation="wave" />
				<Skeleton width="68%" height={22} animation="wave" />
			</div>
			<div style={{marginBottom:25}}>
				<Skeleton variant="rect" width="100%" height={100} animation="wave"  />
				<Skeleton width="95%" height={33} animation="wave" />
				<Skeleton width="68%" height={22} animation="wave" />
			</div>
		</div>
	)
}

export default List;