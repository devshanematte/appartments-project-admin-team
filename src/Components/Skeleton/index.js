import Blocks from './Blocks';
import Full from './Full';
import List from './List';

export default {
	Blocks,
	Full,
	List
}