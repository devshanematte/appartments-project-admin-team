import React, {
	useState
} from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const Header = () => {

	return (
		<div className="col-md-12">
			<header>
				<Link to="/">	
					<h4>Панель управления сервисом Apartments</h4>
				</Link>
			</header>
		</div>
	)
}

export default Header;