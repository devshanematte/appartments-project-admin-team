import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, HashRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import { PersistGate } from 'redux-persist/lib/integration/react';
import createStore from './Services/Store/';

import './Services/Styles/app.css';

//components
import Initial from './Containers/Initial';

const {
	store,
	persistor
} = createStore()

ReactDOM.render((
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <HashRouter>
        <Route path="/" render={() => <Initial /> } />
      </HashRouter>
    </PersistGate>
	</Provider>
), document.getElementById('root'))

serviceWorker.unregister()
