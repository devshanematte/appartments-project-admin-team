import React, { 
	useState,
	useEffect
} from 'react';
import '../../../Services/Styles/auth.css';
import { 
	useDispatch,
	useSelector
} from 'react-redux';
import { api } from '../../../Services';

const Login = () => {

	let dispatch = useDispatch();
	let getAuthState = useSelector((state)=>{
		return state.auth
	});

	let [phoneOrEmail, setPhoneOrEmail] = useState('');
	let [password, setPassword] = useState('');

	const loginAdmin = () => {
		return dispatch(api.auth.login(phoneOrEmail, password));
	}

	useEffect(()=>{

	}, []);

	return (

		<div className="auth-block">
			<div className="container">
				<div className="col-md-4 col-md-offset-8">
					<div className="auth-block-form">

						<div className="form-auth-input">

							<p>Авторизация</p>

							<section>
								<label>Email</label>
								<input onChange={(value)=>{ setPhoneOrEmail(value.target.value) }} type="text" placeholder="Телефон или email"/>
							</section>

							<section>
								<label>Пароль</label>
								<input onChange={(value)=>{ setPassword(value.target.value) }} type="password" placeholder="Пароль"/>
							</section>

							{
								getAuthState.requestLogin ?
									<input type="submit" value="Подождите..."/>
								:
									<input type="submit" onClick={()=>{ loginAdmin() }} value="Войти"/>
							}

						</div>

						<div className="form-auth-footer">
							<p>Apartments 2020 ©</p>
						</div>

					</div>
				</div>
			</div>
		</div>

	)
}

export default Login;