import React, {
	useRef,
	useEffect,
	useState
} from 'react';
import { withRouter } from 'react-router-dom';
import { 
	Header,
	RightSide,
	Skeleton
} from '../../../../Components';
import Dropzone from 'react-dropzone';
import config from '../../../../Config';
import { Link } from 'react-router-dom';
import { 
	AppBar,
	Tabs,
	Tab,
	TextField,
	Select,
	MenuItem,
	InputLabel,
	FormControl,
	Button
} from '@material-ui/core';

import {
	Autorenew,
	HighlightOff,
	DeleteForever
} from '@material-ui/icons';

import { 
	useDispatch, 
	useSelector 
} from 'react-redux';
import { api } from '../../../../Services';

import FlatItem from './Helpers/flatItem';

const AddFloorsAndFlats = ({
	history,
	match
}) => {

	const dispatch = useDispatch();
	const refPoint = useRef();

	const [floor, setFloor] = useState(1);

	const [title, setTitle] = useState('');

	const [copyFloorStatus, setCopyFloorStatus] = useState(false);
	const [copyFloor, setCopyFloor] = useState(1);

	const [statusAreaBlock, setStatusAreaBlock] = useState(false);
	const [blockAreas, setBlockAreas] = useState([]);
	const [areaPoints, setAreaPoints] = useState('');

	const [viewFlat, setViewFlat] = useState(null);

	const [downIndexPointStatus, setDownIndexPointStatus] = useState(false);
	const [downIndexPoint, setDownIndexPoint] = useState(null);

	const getStateBlock = useSelector(state => state.flats);

	const getStateTemplates = useSelector(state => state.flats.templates);
	const [template, setTemplate] = useState('none');

	const getStateFiles = useSelector(state => state.files);

	const [statusEditAreaBlock, setEditStatusAreaBlock] = useState(false);
	const [EditBlockAreas, setEditBlockAreas] = useState([]);
	const [EditAreaPoints, setEditAreaPoints] = useState('');

	useEffect(()=>{

		dispatch({
			type:'CLEAR_TEMPLATES'
		});
		dispatch(api.blocks.getFloorByBlock(match.params.id, match.params.complex_id, floor));
		dispatch(api.flats.getTemplates(0, match.params.complex_id, false));

	}, []);

	const renderItemsFloor = (floors_count, statusCopy) => {

		if(statusCopy){

			let flatsNum = (Number(floors_count) - (Number(floors_count) - Number(floor)))-1;
			let flatArray = [];

			for(let y = 0; y < flatsNum; y++){
				flatArray.push(y);
			}

			return flatArray.map((item, index)=>{
				return <MenuItem style={{fontSize:17}} value={index+1}>{ index+1 }</MenuItem>
			});	

			return
		}

		let flatsNum = Number(floors_count);
		let flatArray = [];

		for(let y = 0; y < flatsNum; y++){
			flatArray.push(y);
		}

		return flatArray.map((item, index)=>{
			return <MenuItem style={{fontSize:17}} value={index+1}>{ index+1 }</MenuItem>
		});

	}

	const uploadPlanFloor = (files) => {
    	dispatch(api.files.uploadFile(files[0], 'plan-floor'));
	}

	const addFloorInfo = () => {

		return dispatch(api.blocks.addFloorInfo(match.params.id, match.params.complex_id, floor, getStateFiles.plan_floor));

	}

	const mathAreaPoints = (areas) => {

		let areaPointsString = '';
		setBlockAreas(areas);
		setEditBlockAreas(areas);
		areas.map((point, index)=>{
			areaPointsString = areaPointsString + `${point[0]}% ${point[1]}%,`;
		});
		
		setAreaPoints(areaPointsString.substring(0, areaPointsString.length - 1));
		setEditAreaPoints(areaPointsString.substring(0, areaPointsString.length - 1))
		return
	}

	const addPoint = (event, type) => {

		let posXnative = event.nativeEvent.offsetX;
		let posYnative = event.nativeEvent.offsetY;
		let blockHeight = document.getElementById('area-block-panorama-select').clientHeight;
		let blockwidth = document.getElementById('area-block-panorama-select').clientWidth;

		let percentX = (posXnative/blockwidth) * 100;
		let percentY = (posYnative/blockHeight) * 100;

		let newBlockAreas;

		if(statusEditAreaBlock){

			newBlockAreas = [...EditBlockAreas, [percentX,percentY]]
			mathAreaPoints(newBlockAreas);

		}else{

			newBlockAreas = [...blockAreas, [percentX,percentY]]
			mathAreaPoints(newBlockAreas);

		}

		return

	}

	const removePointFromAreas = (index) => {

		let resRemovePoint = window.confirm('Вы дейтсвительно хотите удалить точку?');

		if(resRemovePoint){
			let newBlockAreas = []

			if(statusEditAreaBlock){

				EditBlockAreas.map((point, indexPoint)=>{
					if(indexPoint != index){
						newBlockAreas = [...newBlockAreas, point];
					}
				});
				mathAreaPoints(newBlockAreas);

			}else{

				blockAreas.map((point, indexPoint)=>{
					if(indexPoint != index){
						newBlockAreas = [...newBlockAreas, point];
					}
				});
				mathAreaPoints(newBlockAreas);

			}

			return
		}

	}

	const moveMousePoint = (event) => {

		if(downIndexPointStatus){

			let posXnative = event.nativeEvent.offsetX;
			let posYnative = event.nativeEvent.offsetY;
			let blockHeight = document.getElementById('area-block-panorama-select').clientHeight;
			let blockwidth = document.getElementById('area-block-panorama-select').clientWidth;

			let percentX = (posXnative/blockwidth) * 100;
			let percentY = (posYnative/blockHeight) * 100;

			if(statusEditAreaBlock){
				EditBlockAreas[downIndexPoint] = [percentX, percentY];
				mathAreaPoints(EditBlockAreas);
			}else{
				blockAreas[downIndexPoint] = [percentX, percentY];
				mathAreaPoints(blockAreas);
			}

			return

		}

	}

	const createFlatWithPoints = () => {

		dispatch(api.flats.createwithPoints({
			block_id:match.params.id,
			apartment_complex_id:match.params.complex_id,
			area_points:blockAreas,
			area_points_for_css:areaPoints,
			floor,
			title,
			template
		}));

		setStatusAreaBlock(!statusAreaBlock);
		setBlockAreas([]);
		setAreaPoints('');
		setTitle('');

		return

	}

	const copyFloorEvent = () => {

		dispatch(api.flats.copyFloor({
			block_id:match.params.id,
			apartment_complex_id:match.params.complex_id,
			floor,
			copyFloor
		}));

		return

	}

	const deleteFlat = (id) => {

		dispatch(api.flats.delete(id, {
			block_id:match.params.id,
			apartment_complex_id:match.params.complex_id,
			floor
		}));
		return
		
	}

	const saveEditAreaPointsFlat = (idFlat) => {

		setEditStatusAreaBlock(false);
		setEditBlockAreas([]);
		setEditAreaPoints('');

		dispatch(api.flats.updateFlatAreaPoints(
			idFlat,
			EditBlockAreas,
			EditAreaPoints
		));
		return	

	}

	return (
        <div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					{
						getStateBlock.blockInformation ? 

							<div className="col-md-12">
								<div className="row">
									<div className="col-md-3">
										<RightSide/>
									</div>
									<div className="col-md-9">
										<content className="cities-page-block">
											<h1>Управление планировками этажей</h1>
											<p>ЖК { getStateBlock.blockInformation.complex_id.title }, блок { getStateBlock.blockInformation.title }</p>

											<section className="input-section-form" style={{marginTop:15}}>
												<FormControl className='select-option-style'>
										    		<InputLabel style={{fontSize:17}} htmlFor="age-native-simple">Выберите этаж*</InputLabel>
										        	<Select
										          		labelId="demo-simple-select-label"
										          		id="demo-simple-select"
										          		value={floor}
										          		fullWidth
										          		onChange={(val)=>{
										          			setFloor(val.target.value);
										          			dispatch(api.blocks.getFloorByBlock(match.params.id, match.params.complex_id, val.target.value));
										          		}}
										          		style={{fontSize:17}}
										        	>
										        		{ renderItemsFloor(getStateBlock.blockInformation.floors_count) }
										        	</Select>
										    	</FormControl>
								        	</section>

								        	<div className="line-main-full-width"></div>

								        	<section className="input-section-form section-upload-floor-image" style={{marginTop:15}}>
												<h5>Загрузить или изменить фотографию планировки этажа</h5>
												{
													getStateFiles.request_upload_status ?
														<h3>Загружаем</h3>
													:
														<div className="upload-floor-image-block">
															<input type="file" onChange={(val)=>{uploadPlanFloor(val.target.files)}} />
														</div>
												}
												{
													getStateBlock.request_flats_status ?
														<h3>Подождите...</h3>
													: getStateFiles.plan_floor ?
														<div className="plan-preview-photo-floor-main-block">
															<div className="plan-preview-photo-floor" style={{
																background:`url(${getStateFiles.plan_floor.full_url}) left / contain no-repeat`
															}}></div>
															<h5 onClick={()=>{ addFloorInfo(); }}>Прикрепить фотографию</h5>
														</div>
													:
														<div></div>
												}
								        	</section>

								        	<div className="line-main-full-width"></div>

								        	{
								        		getStateBlock.blockInformation.floor_info ?
								        			<div className="section-with-plan-floor">

								        				{
								        					statusAreaBlock ?
								        						<span onClick={()=>{
								        							setStatusAreaBlock(!statusAreaBlock);
								        							setBlockAreas([]);
								   									setAreaPoints('');
								   									return
								        						}}>Сбросить</span>
								        					:
								        						<span onClick={()=>{setStatusAreaBlock(!statusAreaBlock)}}>Добавить квартиру</span>
								        				}

								        				{
								        					Number(floor) > 1 ?
								        						<div>
								        							{
								        								copyFloorStatus ?
											        						<span style={{marginLeft:15}} onClick={()=>{setCopyFloorStatus(!copyFloorStatus)}}>Копировать квартиры с этажа (закрыть)</span>
											        					:
											        						<span style={{marginLeft:15}} onClick={()=>{setCopyFloorStatus(!copyFloorStatus)}}>Копировать квартиры с этажа</span>
								        							}
								        						</div>
								        					:
								        						<div></div>
								        					
								        				}

								        				{
								        					copyFloorStatus && Number(floor) > 1 && (
								        						<div className="copy-floor-block">
								        							<h6>Копирование этажа</h6>
								        							<hr/>
								        							<p>Выберите этаж и нажмите копировать</p>

																	<section className="input-section-form" style={{marginTop:15}}>
																		<FormControl className='select-option-style'>
																    		<InputLabel style={{fontSize:17}} htmlFor="age-native-simple">Выберите этаж*</InputLabel>
																        	<Select
																          		labelId="demo-simple-select-label"
																          		id="demo-simple-select"
																          		value={copyFloor}
																          		fullWidth
																          		onChange={(val)=>{
																          			setCopyFloor(val.target.value);
																          		}}
																          		style={{fontSize:17}}
																        	>
																        		{ renderItemsFloor(getStateBlock.blockInformation.floors_count, true) }
																        	</Select>
																    	</FormControl>
														        	</section>

								        							<button onClick={()=>{copyFloorEvent()}}>Копировать</button>
								        						</div>
								        					)
								        				}

														<section className="blocks-content-visual">
											        		<div className="main-block-with-pamorama">
												        		<div id="main-photo-panorama" className="main-photo-panorama" >

													        		{
												        				viewFlat ?
												        					<div style={{
											        						clipPath: `polygon(${viewFlat.area_points_for_css})`
											        					}} id="area-block-panorama-select" className="area-block-panorama-select-background area-block-panorama-select" ></div>
												        				:
												        					<div></div>
												        			}

												        			<img src={`${config.public}${getStateBlock.blockInformation.floor_info.photo.url}`} />

												        			<div className="event-points-block" onClick={(event)=>{
												        				if(statusAreaBlock || statusEditAreaBlock){
												        					return addPoint(event, 'default');
												        				}
												        			}} onMouseMove={(e)=>{
												        				return moveMousePoint(e);
												        			}}></div>

									        						{
									        							statusAreaBlock && blockAreas && blockAreas.length ?
									        								blockAreas.map((point, index)=>{
									        									return (
			                                                                        <div onDoubleClick={()=>{
			                                                                        	return removePointFromAreas(index);
			                                                                        }} ref={refPoint} onMouseDown={(e)=>{
			                                                                        	setDownIndexPointStatus(true);
			                                                                        	setDownIndexPoint(index);
			                                                                        	return
			                                                                        }} 
			                                                                        onMouseUp={(e)=>{ 
																        				setDownIndexPointStatus(false);
																        				setDownIndexPoint(null);
																        				return
																        			}} 
																        			key={index} className="dot-point-area" style={{
			                                                                            left:`${point[0]}%`,
			                                                                            top:`${point[1]}%`
			                                                                        }}></div>
			                                                                    );
									        								})
									        							:
									        								<div></div>
									        						}

									        						{
									        							statusEditAreaBlock && EditBlockAreas && EditBlockAreas.length ?
									        								EditBlockAreas.map((point, index)=>{
									        									return (
			                                                                        <div onDoubleClick={()=>{
			                                                                        	return removePointFromAreas(index);
			                                                                        }} ref={refPoint} onMouseDown={(e)=>{
			                                                                        	setDownIndexPointStatus(true);
			                                                                        	setDownIndexPoint(index);
			                                                                        	return
			                                                                        }} 
			                                                                        onMouseUp={(e)=>{ 
																        				setDownIndexPointStatus(false);
																        				setDownIndexPoint(null);
																        				return
																        			}} 
																        			key={index} className="dot-point-area" style={{
			                                                                            left:`${point[0]}%`,
			                                                                            top:`${point[1]}%`
			                                                                        }}></div>
			                                                                    );
									        								})
									        							:
									        								<div></div>
									        						}

												        			{
												        				statusAreaBlock ?
												        					<div onMouseMove={(e)=>{
														        				if(statusAreaBlock){
														        					return moveMousePoint(e);
														        				}
														        			}} onClick={(event)=>{
			                                                                        	if(statusAreaBlock){
																	        				return addPoint(event, 'area');
																	        			}
			                                                                        }} id="area-block-panorama-select" className="area-block-panorama-select-background area-block-panorama-select" style={{
												        						clipPath: `polygon(${areaPoints})`
												        					}}></div>
												        				:
												        					<div></div> 
												        			}

												        			{
												        				statusEditAreaBlock ?
												        					<div onMouseMove={(e)=>{
														        				if(statusEditAreaBlock){
														        					return moveMousePoint(e);
														        				}
														        			}} onClick={(event)=>{
			                                                                        	if(statusEditAreaBlock){
																	        				return addPoint(event, 'area');
																	        			}
			                                                                        }} id="area-block-panorama-select" className="area-block-panorama-select-background area-block-panorama-select" style={{
												        						clipPath: `polygon(${EditAreaPoints})`
												        					}}></div>
												        				:
												        					<div></div> 
												        			}

												        		</div>
											        		</div>
											        	</section>

											        	{
											        		statusAreaBlock ?
																<section className="input-section-form" style={{marginTop:15}}>
														        	<TextField 
														        		fullWidth
														        		onChange={(val)=>{setTitle(val.target.value)}}
														        		id="standard-basic" 
														        		label="Номер квартиры*"
														        		inputProps={{style: {
														        			fontSize: 16
														        		}}}
														        		value={title}
														  				InputLabelProps={{style: {fontSize: 16}}}
														        	/>
														        	<Select
														          		labelId="demo-simple-select-label"
														          		id="demo-simple-select"
														          		value={template}
														          		fullWidth
														          		onChange={(val)=>{
														          			setTemplate(val.target.value);
														          		}}
														          		style={{
														          			fontSize:17,
														          			marginTop:15
														          		}}
														        	>
														        		<MenuItem style={{fontSize:17}} value='none'>Выберите шаблон для квартиры</MenuItem>
														        		{
														        			getStateTemplates && getStateTemplates.length ?
														        				getStateTemplates.map((item)=>{
														        					return <MenuItem style={{fontSize:17}} value={item._id}>{ item.title }</MenuItem>
														        				})
														        			:
														        				<MenuItem style={{fontSize:17}}>Шаблонов не найдено</MenuItem>
														        		}
														        	</Select>
													        	</section>
											        		:
											        			<div></div>
											        	}

											        	{
											        		statusAreaBlock ?
											        			<span onClick={()=>{createFlatWithPoints()}}>Добавить квартиру</span>
											        		:
											        			<div></div>
											        	}
								        				
								        			</div>

								        		:
								        			<h5>Планировка этажа не загружена.</h5>
								        	}

								        	<div className="line-main-full-width" style={{
								        		marginTop:20
								        	}}></div>

								        	<div className="list-flats-block" style={{
								        		marginBottom:25
								        	}}>
								        		<h4>Список квартир на этаже {floor}</h4>
								        		{
								        			getStateBlock.blockInformation.flats && getStateBlock.blockInformation.flats.length ?
								        				getStateBlock.blockInformation.flats.map((item, index)=>{
								        					return <div onMouseEnter={() => setViewFlat(item)} onMouseLeave={() => setViewFlat(null)} className="flat-main-item" key={index}>
								        						<FlatItem 
								        							api={api} 
								        							dispatch={dispatch} 
								        							item={item} 
								        							deleteFlat={(id)=>{deleteFlat(id)}} 
								        							editAreaPointsFlat={(points, pointsString, flatId)=>{
																		setEditStatusAreaBlock(true);
																		setEditBlockAreas(points);
																		setEditAreaPoints(pointsString);
								        							}}
								        							saveEditAreaPointsFlat={(flatId)=>saveEditAreaPointsFlat(flatId)}
								        						/>
								        					</div>
								        				})
								        			:
								        				<h5>На данном этаже квартир не найдено</h5>
								        		}
								        	</div>

										</content>
									</div>
								</div>
							</div>

						:
							<div></div>
					}

				</div>
			</div>	

		</div>
    );
}

export default withRouter(AddFloorsAndFlats);