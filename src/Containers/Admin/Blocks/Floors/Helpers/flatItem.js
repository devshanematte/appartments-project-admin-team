import React, {
	useState
} from 'react';
import { Link } from 'react-router-dom';

import {
	DeleteForever,
	FlipToBack
} from '@material-ui/icons';

const FlatItem = ({ 
	item, 
	deleteFlat, 
	api, 
	dispatch,
	editAreaPointsFlat,
	saveEditAreaPointsFlat,
}) => {

	const [editStatus, setEditStatus] = useState(false);
	const [title, setTitle] = useState(item.title);

	const [statusAreaPoints, setStatusAreaPoints] = useState(false);

	const edittitleEvent = () => {
		setEditStatus(false);
		item.title = title;
		return dispatch(api.flats.editTitle(title, item._id));
	}

	return (
		<div className="flat-main-item-child">
			

			{
				editStatus ?
					<div>
						<input onChange={(val)=>{setTitle(val.target.value)}} type="text" blur value={title}/>
						<input onClick={()=>{edittitleEvent()}} type="submit" value="Сохранить"/>
					</div>
				:
					<h6 onClick={()=>{ setEditStatus(true) }}>{ item.title }</h6>
			}

			<div className="flat-main-item-controlls">
				{
					statusAreaPoints ?
						<span onClick={()=>{
							setStatusAreaPoints(false);
							saveEditAreaPointsFlat(item._id);
						}} title="Удалить" className="block-icon-remove-flat button-icon-remove-flat-save-area-points">Сохранить</span>
					:
						<span onClick={()=>{
							setStatusAreaPoints(true);
							editAreaPointsFlat(item.area_points, item.area_points_for_css, item._id);
						}} title="Удалить" className="block-icon-remove-flat"><FlipToBack/></span>
				}
				
				<span onClick={()=>{ deleteFlat(item._id) }} title="Удалить" className="block-icon-remove-flat block-icon-remove"><DeleteForever/></span>
				<Link to={`/apartments/flats/${item.template}/edit`}>Редактирование информации</Link>
			</div>			
		</div>
	)

}

export default FlatItem;