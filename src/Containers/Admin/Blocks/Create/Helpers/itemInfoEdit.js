import React from 'react';

import { 
	Check,
	Delete,
	Image
} from '@material-ui/icons';

const ItemInfoEdit = ({
	changeTitle,
	setBlockTitleInput,
	blockTitleInput,
	setfloors_count,
	changeFloorCount,
	setBlockfloors_countInput,
	deleteBlock,
	changeImageBlock,
	floors_count,
	title,
	blockfloors_countInput,
	setTitle
}) => {
	return(
	  	<div className="handle-content">
	  		<div className="handle-content-info">
	  			<h4>
	  				<small>Секция:</small>
	  				{ 
	  					blockTitleInput ?
	  						<div>
	  							<input onChange={(val)=>{setTitle(val.target.value)}} value={title} type="text"/>
	  							<Check onClick={changeTitle.bind(undefined)}/>
	  						</div>
	  					:
	  						<span onClick={()=>{setBlockTitleInput(true)}}>{ title }</span>
	  				}
	  			</h4>
	  			<h4><small>Этажей:</small> 
	  				{ 
	  					blockfloors_countInput ?
	  						<div>
	  							<input onChange={(val)=>{setfloors_count(val.target.value)}} value={floors_count} type="text"/>
	  							<Check onClick={changeFloorCount(undefined)}/>
	  						</div>
	  					:
	  						<span onClick={()=>{setBlockfloors_countInput(true)}}>{ floors_count }</span>
	  				}
	  			</h4>

	  			<span title="Удалить"  onClick={deleteBlock(undefined)} className="block-remove-item">
	  				<Delete/>
	  			</span>

	  			<span title="Изменить картинку" className="block-change-image-item">
	  				<Image/>
	  				<input type="file" onChange={changeImageBlock(undefined)} />
	  			</span>

	  		</div>
	  	</div>
	)
};

export default ItemInfoEdit;