import React from 'react';

import config from '../../../../../Config';

import { 
	useDispatch, 
	useSelector 
} from 'react-redux';
import { 
	api
} from '../../../../../Services';
import Draggable from 'react-draggable';
import { ReactSVG } from 'react-svg';

import { Link } from 'react-router-dom';

import { 
	Check,
	Delete,
	Image,
	Settings
} from '@material-ui/icons';

const DraggableItem = ({
	block,
	index
}) => {

	const dispatch = useDispatch();

	const [blockTitleInput, setBlockTitleInput] = React.useState(false);
	const [blockfloors_countInput, setBlockfloors_countInput] = React.useState(false);
	const [title, setTitle] = React.useState(block.title);
	const [floors_count, setfloors_count] = React.useState(block.floors_count);
	const [modalSettingBlockStatus, setModalSettingBlockStatus] = React.useState(false);

	const changeTitle = () => {

		setBlockTitleInput(false);
		return dispatch(api.blocks.changeTitle(title, block._id));

	}

	const changeFloorCount = () => {
		setBlockfloors_countInput(false);
		return dispatch(api.blocks.changeFloorCount(floors_count, block._id));
	}

	const deleteBlock = () => {

		let confirmDelete = window.confirm('Вы действительно хотите удалить блок?');

		if(confirmDelete){
			return dispatch(api.blocks.deleteBlock(block._id, block.complex_id));
		}

		return

	}

	const changeImageBlock = (files) => {
		return dispatch(api.blocks.changeImageBlock(block._id, block.complex_id, files.target.files[0]));
	}

	return(
      	<Draggable
      		key={index}
        	handle=".handle"
        	className="Draggable-style"
        	defaultPosition={{x: 0, y: 0}}
        	position={{
        		x:block.photo_points.x, 
        		y: block.photo_points.y
        	}}
        	onStart={(e)=>{
        		//console.log('onStart', e)
        	}}
        	onDrag={(e, data)=>{
        		//console.log('onDrag', data)
        	}}
        	onStop={(e, data)=>{
        		//console.log('onStop', e);
        		dispatch({
        			type:'UPDATE_POINTS_BLOCK_APARTMENT_COMPLEX',
        			x:data.x,
        			y:data.y,
        			index
        		});
        		dispatch(api.blocks.updateInformationBlock(data.x, data.y, block._id));
        	}}>
	        	<div className="block-image-map-style-item handle" >

	        		<Settings className="setting-icon-block" onClick={()=>{setModalSettingBlockStatus(!modalSettingBlockStatus)}} />
	        		<ReactSVG className="wrapper-class-name" src={config.public + block.photo.url} />

	        	  	<div className={ modalSettingBlockStatus ? "handle-content handle-content-openned" : "handle-content" }>
	        	  		<div className="handle-content-info">
	        	  			<h4>
	        	  				<small>Секция:</small>
	        	  				{ 
	        	  					blockTitleInput ?
	        	  						<div>
	        	  							<input onChange={(val)=>{setTitle(val.target.value)}} value={title} type="text"/>
	        	  							<Check onClick={changeTitle.bind(undefined)}/>
	        	  						</div>
	        	  					:
	        	  						<span onClick={()=>{setBlockTitleInput(true)}}>{ title }</span>
	        	  				}
	        	  			</h4>
	        	  			<h4><small>Этажей:</small> 
	        	  				{ 
	        	  					blockfloors_countInput ?
	        	  						<div>
	        	  							<input onChange={(val)=>{setfloors_count(val.target.value)}} value={floors_count} type="text"/>
	        	  							<Check onClick={changeFloorCount.bind(undefined)}/>
	        	  						</div>
	        	  					:
	        	  						<span onClick={()=>{setBlockfloors_countInput(true)}}>{ floors_count }</span>
	        	  				}
	        	  			</h4>

	        	  			<span title="Удалить"  onClick={deleteBlock.bind(undefined)} className="block-remove-item">
	        	  				<Delete/>
	        	  			</span>

	        	  			<span title="Изменить картинку" className="block-change-image-item">
	        	  				<Image/>
	        	  				<input type="file" onChange={changeImageBlock.bind(undefined)} />
	        	  			</span>

	        	  			<div className="link-flats-block">
	        	  				<Link to={`/apartments/flats/${block._id}/add`}>Квартиры</Link>
	        	  			</div>

	        	  		</div>

	        	  	</div>

	        	</div>
      	</Draggable>
	)

}

export default DraggableItem;