import React, {
	useState
} from 'react';
import { Link } from 'react-router-dom';
import {
	DeleteForever,
	Edit,
	FlipToBack
} from '@material-ui/icons';

const ItemBlock = ({
	item,
	match,
	setViewBlock,
	deleteBlock,
	api,
	dispatch,
	editPointPlan,
	saveEditAreaPointBlock
}) => {

	const [editBlock, setEditBlock] = useState(false);

	const [statusEditAreaPointBlock, setStatusEditAreaPointBlock] = useState(false);

	const [title, setTitle] = useState(item.title);
	const [floors_count, setFloors_count] = useState(item.floors_count);

	const editBlockService = () => {

		if(!title && !floors_count){
			setTitle(item.title);
			setFloors_count(item.floors_count);
		}

		setEditBlock(false);
		dispatch(api.apartments.editBlockApartment(title, floors_count, item._id));
		return

	}

	return (
		<div onMouseEnter={() => setViewBlock(item)} onMouseLeave={() => setViewBlock(null)} className="block-list-item block-list-item-block">
			{
				editBlock ?
					<div className="block-list-item-flex-form">
						<p>Название блока</p>
						<input onChange={(value)=> setTitle(value.target.value)} type='text' value={title} />
						<p>Количество этажей</p>
						<input onChange={(value)=> setFloors_count(value.target.value)} type='text' value={floors_count} />
						<input type='submit' onClick={()=> editBlockService()} value="Сохранить" />
					</div>
				: statusEditAreaPointBlock ?
					<div className="block-list-item-flex-form" style={{
						flexDirection:'row',
						justifyContent:'space-between'
					}}>
						<p>{title}</p>
						<input type='submit' className="block-list-item-flex-form-save" onClick={()=> {
							saveEditAreaPointBlock(item._id);
							setStatusEditAreaPointBlock(false);
						}} value="Сохранить" />
					</div>	
				:
					<div className="block-list-item-flex">
						<p>{ title }</p>
						<div className="block-list-item-flex-button-control">
							<span onClick={()=> {
								setStatusEditAreaPointBlock(true);
								editPointPlan(item.area_points, item.area_points_string)
							}} className="edit-block-button"><FlipToBack/></span>
							<span onClick={()=>setEditBlock(!editBlock)} className="edit-block-button"><Edit/></span>
							<span onClick={()=>{ deleteBlock(item._id) }} title="Удалить" className="block-icon-remove"><DeleteForever/></span>
							<Link to={`/apartments/blocks/${item._id}/add-floors/${match.params.id}`}>Этажи и квартиры</Link>
						</div>
					</div>	
			}
		</div>
	)
}

export default ItemBlock;