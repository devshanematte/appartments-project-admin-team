import React from 'react';
import { withRouter } from 'react-router-dom';
import { 
	Header,
	RightSide,
	Skeleton
} from '../../../../Components';
import Dropzone from 'react-dropzone';
import config from '../../../../Config';

import { 
	AppBar,
	Tabs,
	Tab,
	TextField,
	Select,
	MenuItem,
	InputLabel,
	FormControl,
	Button
} from '@material-ui/core';

import {
	Autorenew
} from '@material-ui/icons';

import { 
	useDispatch, 
	useSelector 
} from 'react-redux';
import { api } from '../../../../Services';
import Draggable from 'react-draggable';
import DraggableItem from './Helpers/DraggableItem';

const CreateBlocks = ({
	history,
	match
}) => {

	const dispatch = useDispatch();

	const getStateApartments = useSelector(state => state.apartments);
	const getStateFiles = useSelector(state => state.files);

	const [title, setTitle] = React.useState('');
	const [floors, setFloors] = React.useState('');
	const [complex, setComplex] = React.useState('');

	React.useEffect(()=>{

		dispatch(api.apartments.getApartmentById(match.params.id, true));

	}, []);

  	const handleChangeComplex = (event) => {
    	setComplex(event.target.value);
  	};

  	const handleDrop = (files) => {
    	dispatch(api.files.uploadFile(files[0], 'block-image'));
	}

	const addBlock = () => {

		let block = {
			title,
			photo: getStateFiles.photo_block ? getStateFiles.photo_block : null,
			floors_count:floors,
			complex_id:match.params.id,
			photo_points:{
		      	x:0,
		      	y:0
			}
		};

		return dispatch(api.blocks.addBlock(block));

	}

	const resetBlocks = () => {
		return dispatch({
			type:'RESET_APARTMENT_BLOCKS'
		})
	}

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					{
						getStateApartments.getApartmentForEdit ? 

							<div className="col-md-12">
								<div className="row">
									<div className="col-md-3">
										<RightSide/>
									</div>
									<div className="col-md-9">
										<content className="cities-page-block">
											<h1>Управление блоками</h1>
											<p>{getStateApartments.getApartmentForEdit.title}</p>

											<section className="input-section-form" style={{marginTop:15}}>
									        	<TextField 
									        		fullWidth
									        		onChange={(val)=>{setTitle(val.target.value)}}
									        		id="standard-basic" 
									        		label="Название блока*"
									        		inputProps={{style: {
									        			fontSize: 16
									        		}}}
									        		value={title}
									  				InputLabelProps={{style: {fontSize: 16}}}
									        	/>
								        	</section>

											<section className="input-section-form" style={{marginTop:15}}>
									        	<TextField 
									        		fullWidth
									        		onChange={(val)=>{setFloors(val.target.value)}}
									        		id="standard-basic" 
									        		label="Количество этажей*"
									        		inputProps={{style: {
									        			fontSize: 16
									        		}}}
									        		value={floors}
									  				InputLabelProps={{style: {fontSize: 16}}}
									        	/>
								        	</section>

								        	<section style={{
								        		marginBottom:15
								        	}} className="upload-photo-block">
								        		<h1>Добавьте фотографию блока</h1>

								        		<div className="section-block-photos">
													<Dropzone onDrop={handleDrop.bind(undefined)}>
													  	{({getRootProps, getInputProps}) => (
														    <section className="drag-photo-complex">
														    	{
														    		getStateFiles.request_upload_status ?
														    			<div>
														    				<Autorenew size={55} className="spin" />
														    			</div>
														    		:
																      	<div {...getRootProps()}>
																        	<input {...getInputProps()} />
																        	<p>Для загрузки фотографии можете перенести файл сюда или нажать на область</p>
																      	</div>
														    	}
														    </section>
													  	)}
													</Dropzone>

													{
														getStateFiles.photo_block ? 
															<div className="photo-apartment-url" style={{
																background:`url(${getStateFiles.photo_block.full_url}) center / cover no-repeat`
															}}></div>
														:
															<div></div>
													}

												</div>

								        	</section>

								        	<section>
			        							<Button variant="contained" onClick={addBlock.bind(undefined)}>Добавить</Button>
								        	</section>

								        	<div className="devider-block"></div>

								        	<section className="blocks-content-visual">
								        		<h1>Блоки</h1>

								        		{
								        			getStateApartments.getApartmentForEdit.blocks.length ?
											        	<section>
						        							<Button variant="contained" onClick={resetBlocks.bind(undefined)}>Сбросить позиции блоков</Button>
											        	</section>
											        :
											        	<div></div>
								        		}

								        		{
								        			getStateApartments.request_update_blocks ?
								        				<div className="update-status-block-for-blocks-apartments">
								        					<h4>Обновляем информацию. Подождите...</h4>
								        				</div>
								        			:
								        				<div></div>
								        		}

								        		{
								        			getStateApartments.getApartmentForEdit.blocks.length ?

										        		<div className="blocks-form-content">


															<div className="box drag-block-content" >
													          	<div className="drag-block-content-child">
													          		{
													          			getStateApartments.getApartmentForEdit.blocks.map((block, index)=>{
													          				return <DraggableItem key={index} block={block} index={index}/>
													          			})
													          		}

													          	</div>
													        </div>

										        		</div>
										        	:
										        		<div>
										        			<h2>Добавьте несколько блоков</h2>
										        		</div>
								        		}

								        	</section>

										</content>
									</div>
								</div>
							</div>

						:
							<div></div>
					}

				</div>
			</div>	

		</div>
	)
}

export default withRouter(CreateBlocks);