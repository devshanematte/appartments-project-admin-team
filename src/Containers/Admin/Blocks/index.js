import React from 'react';

import { 
	Header,
	RightSide,
	Skeleton
} from '../../../Components';

import { 
	AppBar,
	Tabs,
	Tab
} from '@material-ui/core';

const Blocks = () => {

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<content className="cities-page-block">
									<h1>Блоки</h1>

								</content>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default Blocks;