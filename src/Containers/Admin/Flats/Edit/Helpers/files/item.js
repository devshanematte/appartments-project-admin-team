import React, {
	useState
} from 'react';

import { 
	useDispatch, 
	useSelector 
} from 'react-redux';

import CheckLoadedFile from './checkLoadedFile';

const FilItem = ({
	title,
	NotificationManager,
	api,
	paidTitleOne,
	paidTitleTwo,
	freetitle,
	paidTitleOneTypeForDispatch,
	paidTitleTwoTypeForDispatch,
	freetitleTypeForDispatch
}) => {

	const [file, setFile] = useState(null);
	const [price, setPrice] = useState('');

	const dispatch = useDispatch();
	const getFlatsState = useSelector(state => state.flats);
	const getFilesState = useSelector(state => state.files);

	const uploadFile = () => {

		if(!file){
			return NotificationManager.warning('Выберите файл');
		}else if(file.paid){
			if(!price){
				return NotificationManager.warning('Укажите стоимость');
			}
		}

		dispatch(api.files.uploadFileFlat(file, price));

  		setPrice('');
  		return	
	}

	return (
		<div>
			
			<section>
				<h4>{title}</h4>

				<div>
					{
						paidTitleOne && (
							<div className="paid-block">
								<h3>{ paidTitleOne }</h3>
						      	<input
						        	accept="zip"
						        	id="contained-button-file"
						        	type="file"
						        	onChange={(val)=>{setFile({
						        		file:val.target.files[0],
						        		paid:true,
						        		type:paidTitleOneTypeForDispatch
						        	})}}
						      	/>
						      	<input type="text" value={price} onChange={(val)=>{setPrice(val.target.value)}} className="price-file-input" placeholder="Стоимость файла"/>

						      	{
						      		getFilesState.request_upload_status ?
						      			<input type="submit" className="button-upload-file" value="Подождите..."/>
						      		:
						      			<input type="submit" onClick={()=>{uploadFile()}} className="button-upload-file" value="Загрузить"/>
						      	}
						      	
						      	<CheckLoadedFile state={getFilesState} type={paidTitleOneTypeForDispatch} />

							</div>
						)						
					}
				</div>

				<div>
					{
						paidTitleTwo && (
							<div className="paid-block">
								<h3>{ paidTitleTwo }</h3>
						      	<input
						        	accept="zip"
						        	id="contained-button-file"
						        	type="file"
						        	onChange={(val)=>{setFile({
						        		file:val.target.files[0],
						        		paid:true,
						        		type:paidTitleTwoTypeForDispatch
						        	})}}
						      	/>
						      	<input type="text" value={price} onChange={(val)=>{setPrice(val.target.value)}} className="price-file-input" placeholder="Стоимость файла"/>

						      	{
						      		getFilesState.request_upload_status ?
						      			<input type="submit" className="button-upload-file" value="Подождите..."/>
						      		:
						      			<input type="submit" onClick={()=>{uploadFile()}} className="button-upload-file" value="Загрузить"/>
						      	}
						      	
						      	<CheckLoadedFile state={getFilesState} type={paidTitleTwoTypeForDispatch} />

							</div>
						)
					}
				</div>

				<div>
					{
						freetitle && (
							<div className="free-block">
								<h3>{ freetitle }</h3>
						      	<input
						        	accept="zip"
						        	id="contained-button-file"
						        	type="file"
						        	onChange={(val)=>{setFile({
						        		file:val.target.files[0],
						        		paid:false,
						        		type:freetitleTypeForDispatch
						        	})}}
						      	/>
						      	{
						      		getFilesState.request_upload_status ?
						      			<input type="submit" className="button-upload-file" value="Подождите..."/>
						      		:
						      			<input type="submit" onClick={()=>{uploadFile()}} className="button-upload-file" value="Загрузить"/>
						      	}

						      	<CheckLoadedFile state={getFilesState} type={freetitleTypeForDispatch} />
						      	
							</div>
						)
					}
				</div>

			</section>
			
		</div>
	)
}

export default FilItem;