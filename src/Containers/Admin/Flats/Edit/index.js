import React, {
	useEffect,
	useState
} from 'react';
import { withRouter } from 'react-router-dom';
import { 
	Header,
	RightSide,
	Skeleton
} from '../../../../Components';
import { ReactSVG } from 'react-svg';
import { 
	useDispatch, 
	useSelector 
} from 'react-redux';
import { api } from '../../../../Services';
import config from '../../../../Config';

import { 
	AppBar,
	Tabs,
	Tab,
	TextField,
	Select,
	MenuItem,
	InputLabel,
	FormControl,
	Button,
	InputAdornment
} from '@material-ui/core';
import Dropzone from 'react-dropzone';

import { 
	Delete
} from '@material-ui/icons';

import FilesFlat from '../Create/Helpers/files/';

const EditFlat = ({
	history,
	match
}) => {

	const [title, setTitle] = useState('');
	const [squareMetres, setSquareMetres] = useState('');
	const [countRooms, setCountRooms] = useState('');
	const [materialPrice, setMaterialPrice] = useState('');
	const [totalPrice, setTotalPrice] = useState('');
	const [squareMetrePrice, setSquareMetrePrice] = useState('');
	const [ceilingHeight, setCeilingHeight] = useState(''); 

	const [infoFlat, setInfoFlat] = useState(null); 

	const dispatch = useDispatch();
	const getFlatsState = useSelector(state => state.flats);
	const getFilesState = useSelector(state => state.files);

	const getApartmentsState = useSelector(state => state.apartments);
	const [apartmentComplex, setApartmentComplex] = useState('none'); 

	useEffect(() => {

		dispatch(api.apartments.getApartmentsComplex());
		loadAndUpdate();

	}, []);

	const clearInformation = () => {

		setInfoFlat(null);

		setTitle('');
		setSquareMetres('');
		setCountRooms('');
		setMaterialPrice('');
		setTotalPrice('');
		setSquareMetrePrice('');

		dispatch({
			type:'MATERIAL_BUDGET_EXCEL',
			id:null
		});
		dispatch({
			type:'MATERIAL_BUDGET_PDF',
			id:null
		});	
		dispatch({
			type:'FLAT_ARRANGEMENT_AUTOCAD',
			id:null
		});	
		dispatch({
			type:'APARTMENT_MODEL_3DMAX',
			id:null
		});	
		dispatch({
			type:'APARTMENT_DESIGN_REVIT',
			id:null
		});	
		dispatch({
			type:'APARTMENT_DESIGN_PDF',
			id:null
		});	

		dispatch({
			type:'PLAN_FLAT',
			photo:null
		});

		dispatch({
			type:'APARTMENT_INTERIOR_ARRAY',
			photos:null
		});

		return

	}

	const loadAndUpdate = async () => {

		clearInformation();

		let data = await dispatch(api.flats.getTemplate(match.params.id));

		if(data.status){
			updateInformation(data);
		}

	}

	const updateInformation = (data) => {

		setInfoFlat(data);

		setTitle(data.title);
		setSquareMetres(data.square_metres);
		setCountRooms(data.count_rooms);
		setMaterialPrice(data.material_price);
		setTotalPrice(data.total_price);
		setSquareMetrePrice(data.square_metre_price);
		setCeilingHeight(data.ceiling_height ? data.ceiling_height : '');
		setApartmentComplex(data.apartment_complex_id ? data.apartment_complex_id : 'none');

		data.files.map((file)=> {

			if(file && file.type){

				if(file.type == 'MATERIAL_BUDGET_EXCEL'){
					dispatch({
						type:'MATERIAL_BUDGET_EXCEL',
						id:file._id
					});
				}else if(file.type == 'MATERIAL_BUDGET_PDF'){
					dispatch({
						type:'MATERIAL_BUDGET_PDF',
						id:file._id
					});	
				}else if(file.type == 'FLAT_ARRANGEMENT_AUTOCAD'){
					dispatch({
						type:'FLAT_ARRANGEMENT_AUTOCAD',
						id:file._id
					});	
				}else if(file.type == 'APARTMENT_MODEL_3DMAX'){
					dispatch({
						type:'APARTMENT_MODEL_3DMAX',
						id:file._id
					});	
				}else if(file.type == 'APARTMENT_DESIGN_REVIT'){
					dispatch({
						type:'APARTMENT_DESIGN_REVIT',
						id:file._id
					});	
				}else if(file.type == 'APARTMENT_DESIGN_PDF'){
					dispatch({
						type:'APARTMENT_DESIGN_PDF',
						id:file._id
					});	
				}

				return

			}

			return

		})

		let planPhotoURL = data.plan_photo && data.plan_photo.url ? data.plan_photo.url : '';

		dispatch({
			type:'PLAN_FLAT',
			photo:{
				url:planPhotoURL,
				full_url:config.public + planPhotoURL,
				id:data.plan_photo && data.plan_photo._id ? data.plan_photo._id : ''
			}
		});

		let previewPhotoURL = data.preview_photo && data.preview_photo.url ? data.preview_photo.url : '';

		dispatch({
			type:'APARTMENT_PREVIEW_PHOTO',
			photo:{
				url:previewPhotoURL,
				full_url:config.public + previewPhotoURL,
				id:data.preview_photo && data.preview_photo._id ? data.preview_photo._id : ''
			}
		});

		return

	}

	const updateTemplate = () => {
		
		let fields = {
			title,
			square_metres:squareMetres,
			count_rooms:countRooms,
			material_price:materialPrice,
			total_price:totalPrice,
			square_metre_price:squareMetrePrice,
			ceiling_height:ceilingHeight,
			files: [
				getFilesState.MATERIAL_BUDGET_EXCEL, 
				getFilesState.MATERIAL_BUDGET_PDF,
				getFilesState.FLAT_ARRANGEMENT_AUTOCAD,
				getFilesState.APARTMENT_MODEL_3DMAX,
				getFilesState.APARTMENT_DESIGN_REVIT,
				getFilesState.APARTMENT_DESIGN_PDF,
			],
			plan_photo:getFilesState.PLAN_FLAT,
			id:infoFlat._id,
			preview_photo:getFilesState.PHOTO_PREVIEW,
			apartmentComplex
		}

		return dispatch(api.flats.updateTemplate(fields));

	}

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<content className="cities-page-block">
									{
										getFlatsState.request_flats_status ?
											<h3>Обновляем информацию...</h3>
										: infoFlat ?
											<div className="create-flat-block-form">
												<div className="header-content-page">
													<h1>Редактирование шаблона</h1>
												</div>
												<div className="form-flat-block">
													<div className="row">
														<div className="col-md-5">

															<section className="input-section-form" style={{marginTop:15}}>
													        	<TextField 
													        		fullWidth
													        		onChange={(val)=>{setTitle(val.target.value)}}
													        		id="standard-basic"
													        		label="Название*"
													        		inputProps={{style: {
													        			fontSize: 16
													        		}}}
													        		value={title}
													  				InputLabelProps={{style: {fontSize: 16}}}
													        	/>
												        	</section>

												        	<section className="input-section-form" style={{marginTop:15}}>
												        		<TextField
														          label="Квадратные метры"
														          onChange={(val)=>{setSquareMetres(val.target.value)}}
														          value={squareMetres}
														          placeholder="0"
														          id="standard-start-adornment"
														          InputLabelProps={{style: {fontSize: 16}}}
														          fullWidth
														          type="number"
														          InputProps={{
														            startAdornment: <InputAdornment position="start">кв.м.</InputAdornment>,
														          }}
														        />
												        	</section>

												        	<section className="input-section-form" style={{marginTop:15}}>
												        		<TextField
														          label="Количество комнат"
														          onChange={(val)=>{setCountRooms(val.target.value)}}
														          value={countRooms}
														          placeholder="0"
														          id="standard-start-adornment"
														          InputLabelProps={{style: {fontSize: 16}}}
														          fullWidth
														          type="number"
														          InputProps={{
														            startAdornment: <InputAdornment position="start">кол-во комнат</InputAdornment>,
														          }}
														        />
												        	</section>

												        	<section className="input-section-form" style={{marginTop:15}}>
												        		<TextField
														          label="Стоимость"
														          onChange={(val)=>{setTotalPrice(val.target.value)}}
														          value={totalPrice}
														          placeholder="0"
														          id="standard-start-adornment"
														          InputLabelProps={{style: {fontSize: 16}}}
														          fullWidth
														          type="number"
														          InputProps={{
														            startAdornment: <InputAdornment position="start">тг.</InputAdornment>,
														          }}
														        />
												        	</section>

												        	<section className="input-section-form" style={{marginTop:15}}>
												        		<TextField
														          label="Стоимость за квадратный метр"
														          onChange={(val)=>{setSquareMetrePrice(val.target.value)}}
														          value={squareMetrePrice}
														          placeholder="0"
														          id="standard-start-adornment"
														          InputLabelProps={{style: {fontSize: 16}}}
														          fullWidth
														          type="number"
														          InputProps={{
														            startAdornment: <InputAdornment position="start">тг.</InputAdornment>,
														          }}
														        />
												        	</section>

												        	<section className="input-section-form" style={{marginTop:15}}>
												        		<TextField
														          label="Высота потолка"
														          onChange={(val)=>{setCeilingHeight(val.target.value)}}
														          value={ceilingHeight}
														          placeholder="0"
														          id="standard-start-adornment"
														          InputLabelProps={{style: {fontSize: 16}}}
														          fullWidth
														          type="number"
														          InputProps={{
														            startAdornment: <InputAdornment position="start">м.</InputAdornment>,
														          }}
														        />
												        	</section>

												        	<section className="input-section-form" style={{marginTop:15}}>
												        		<TextField
														          label="Стоимость за материал"
														          onChange={(val)=>{setMaterialPrice(val.target.value)}}
														          value={materialPrice}
														          placeholder="0"
														          id="standard-start-adornment"
														          InputLabelProps={{style: {fontSize: 16}}}
														          fullWidth
														          type="number"
														          InputProps={{
														            startAdornment: <InputAdornment position="start">тг.</InputAdornment>,
														          }}
														        />
												        	</section>

												        	<section className="input-section-form" style={{marginTop:15}}>
												        		{
												        			getApartmentsState.request_apartments_status ?
												        				<h6>Загружается список жилых комплексов</h6>
												        			: getApartmentsState.apartments ?
																		<Select
															          		labelId="demo-simple-select-label"
															          		id="demo-simple-select"
															          		value={apartmentComplex}
															          		fullWidth
															          		onChange={(val)=>{
															          			setApartmentComplex(val.target.value);
															          		}}
															          		style={{
															          			fontSize:17,
															          			marginTop:15
															          		}}
															        	>
															        		<MenuItem style={{fontSize:17}} value='none'>Выберите жилой комплекс</MenuItem>
														        			{
														        				getApartmentsState.apartments.map((item)=>{

														        					return <MenuItem style={{fontSize:17}} value={item._id}>{ item.title }</MenuItem>

														        				})
														        			}
															        	</Select>
												        			:
												        				<h5>Жилых комплексов не найдено</h5>
												        		}
												        	</section>

												        	<div className="line-block"></div>

												        	<FilesFlat />

														</div>
														<div className="col-md-7">
															<div className="plan-block-file">
																<h4>Планировка квартиры</h4>
																{
																	getFilesState.request_upload_status ?
																		<h5>Подождите...</h5>
																	:
																		<input type="file" onChange={(val)=>{
																			dispatch(api.files.uploadFile(val.target.files[0], 'svg-image'));
																		}}/>
																}
																{
																	getFilesState.PLAN_FLAT ?
																		<ReactSVG className="wrapper-class-name" src={config.public + getFilesState.PLAN_FLAT.url} />
																	:
																		<div></div>
																}
															</div>
															{
																/*
															<div className="plan-block-file">
																<h4>Интерьер квартиры</h4>
																{
																	getFilesState.request_upload_status ?
																		<h5>Подождите...</h5>
																	:
																		<input type="file" onChange={(val)=>{
																			dispatch(api.files.uploadFile(val.target.files[0], 'IMAGE'));
																		}}/>
																}
																<div className="images-block-interior-flat">
																	{
																		getFilesState.APARTMENT_INTERIOR ?
																			getFilesState.APARTMENT_INTERIOR.map((item, index)=>{
																				return <div key={index} className="image-interior-flat" style={{background:`url(${item.full_url}) center / cover no-repeat`}}>
																					<span onClick={()=>{
																						dispatch({
																							type:'DELETE_IMAGE_APARTMENT_INTERIOR',
																							index
																						});
																					}}><Delete/></span>
																				</div>
																			})
																		:
																			<div><p>Фотографий нет</p></div>
																	}
																</div>
															</div>
																*/
															}
															<div className="plan-block-file">
																<h4>Превью квартиры</h4>
																{
																	getFilesState.request_upload_status ?
																		<h5>Подождите...</h5>
																	:
																		<input type="file" onChange={(val)=>{
																			dispatch(api.files.uploadFile(val.target.files[0], 'IMAGE_PREVIEW'));
																		}}/>
																}
																{
																	getFilesState.PHOTO_PREVIEW ?
																		<div className="image-interior-flat" style={{background:`url(${config.public}${getFilesState.PHOTO_PREVIEW.url}) center / cover no-repeat`}}></div>
																	:
																		<div></div>
																}
															</div>
														</div>
													</div>
												</div>

												<Button variant="contained" onClick={updateTemplate.bind(undefined)}>Редактировать информацию</Button>

											</div>
										:
											<h3>Информация не валидна</h3>
									}
								</content>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default withRouter(EditFlat);