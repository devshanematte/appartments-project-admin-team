import React, {
	useMemo,
	useEffect
} from 'react';
import { ReactSVG } from 'react-svg';

const ItemSvg = ({
	src
}) => {

	useEffect(()=>{

	}, []);

	return (
		<ReactSVG 
			className="wrapper-class-name inject-me" 
			src={src} 
			evalScripts="never"
		  	fallback={() => <span>Error!</span>}
		  	loading={() => <span>Loading</span>}
		  	renumerateIRIElements={true}
		/>
	);
}

export default ItemSvg;