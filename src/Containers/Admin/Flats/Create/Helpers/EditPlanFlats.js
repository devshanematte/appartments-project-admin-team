import React, {
	useState,
	useMemo,
	useEffect
} from 'react';

import Draggable from 'react-draggable';
import { ReactSVG } from 'react-svg';
import config from '../../../../../Config';

import { useSelector } from 'react-redux';

import {
	Select,
	MenuItem,
	InputLabel,
	FormControl,
	Button
} from '@material-ui/core';

import ItemSvg from './itemSVG';

const EditPlanFlats = ({
	dispatch,
	api,
	idBlock
}) => {

	const resetFlats = () => {
		return dispatch({
			type:'RESET_FLATS_POINTS'
		})
	}

	const [flatNumber, setFlatNumber] = useState('');  

	const getFlatsState = useSelector(state => state.flats);
	const flats = getFlatsState.flatsForBlock;

	const renderItemsFlats = () => {

		let flatsNum = Number(getFlatsState.blockInformation.floors_count);
		let flatArray = [];

		for(let y = 0; y < flatsNum; y++){
			flatArray.push(y);
		}

		return flatArray.map((item, index)=>{
			return <MenuItem style={{fontSize:17}} value={index+1}>{ index+1 }</MenuItem>
		});

	}

	return useMemo(()=>(
		<div>
			<h1>Планировка этажа</h1>
			<Button variant="contained" onClick={resetFlats.bind(undefined)}>Сбросить позиции квартир</Button>

        	<section className="input-section-form" style={{marginTop:15}}>
				<FormControl className='select-option-style'>
		    		<InputLabel style={{fontSize:17}} htmlFor="age-native-simple">Выберите этаж*</InputLabel>
		        	<Select
		          		labelId="demo-simple-select-label"
		          		id="demo-simple-select"
		          		value={flatNumber}
		          		fullWidth
		          		onChange={(val)=>{
		          			setFlatNumber(val.target.value);
		          			dispatch(api.flats.getFlatsForBlock(idBlock, val.target.value));
		          			return
		          		}}
		          		style={{fontSize:17}}
		        	>
		        		{ renderItemsFlats() }
		        	</Select>
		    	</FormControl>
        	</section>

			<div className="blocks-form-content">
				<div className="box drag-block-content" >
		          	<div className="drag-block-content-child drag-block-content-child-flats">

						{
							flats && flats.length ?
								flats.map((flat, index)=>{
									return (
										<Draggable
								      		key={index}
								        	handle=".handle"
								        	className="Draggable-style"
								        	defaultPosition={{x: 0, y: 0}}
								        	position={{
								        		x:flat.plan_photo_points.x, 
								        		y: flat.plan_photo_points.y
								        	}}
								        	onStart={(e)=>{
								        		//console.log('onStart', e)
								        	}}
								        	onDrag={(e, data)=>{
								        		//console.log('onDrag', data)
								        	}}
								        	onStop={(e, data)=>{
								        		//console.log('onStop', e);
								        		
								        		dispatch({
								        			type:'UPDATE_POINTS_FLAT_FOR_BLOCK',
								        			x:data.x,
								        			y:data.y,
								        			index
								        		});
								        		
								        		dispatch(api.flats.updatePointsPlanSVG(data.x, data.y, flat._id, flat.floor));
								        		
								        	}}>
									        	<div className="block-image-map-style-item handle" >
									        		<ItemSvg key={index} index={index} src={config.public + flat.plan_photo.url} />
									        	</div>
								      	</Draggable>
									)
								})
						    :
						    	<div>
						    		<h6>Квартир нет</h6>
						    	</div>
						}

		          	</div>
		        </div>
		    </div>
		</div>
	), [flats, flatNumber])

}
export default EditPlanFlats;