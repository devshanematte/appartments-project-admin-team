import React, {
	useState
} from 'react';

import { 
	Button,
	IconButton
} from '@material-ui/core';

import {
	PhotoCamera
} from '@material-ui/icons';
import { api } from '../../../../../../Services';
import { NotificationManager } from 'react-notifications';

import FilItem from './item';

const FilesFlat = () => {

	return (
		<div className="block-flat-files">
			<h5>Файлы</h5>
			<h6>Все файлы загружаются в формате zip</h6>

			<FilItem api={api} paidTitleOne="Excel" paidTitleOneTypeForDispatch="MATERIAL_BUDGET_EXCEL" freetitleTypeForDispatch="MATERIAL_BUDGET_PDF" freetitle="PDF" NotificationManager={NotificationManager} title="БЮДЖЕТ НА МАТЕРИАЛ" />

			<FilItem api={api} paidTitleOne="Autocad" paidTitleOneTypeForDispatch="FLAT_ARRANGEMENT_AUTOCAD" NotificationManager={NotificationManager} title="ПЛАНИРОВКА КВАРТИРЫ" />

			<FilItem api={api} paidTitleOne="3ds max" paidTitleOneTypeForDispatch="APARTMENT_MODEL_3DMAX" NotificationManager={NotificationManager} title="3Д МОДЕЛЬ КВАРТИРЫ" />

			<FilItem api={api} paidTitleOne="Revit" paidTitleOneTypeForDispatch="APARTMENT_DESIGN_REVIT" paidTitleTwo="PDF" paidTitleTwoTypeForDispatch="APARTMENT_DESIGN_PDF" NotificationManager={NotificationManager} title="РАБОЧИЙ ПРОЕКТ КВАРТИРЫ" />

		</div>
	)
}

export default FilesFlat;