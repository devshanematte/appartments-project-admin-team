import React from 'react';

const CheckLoadedFile = ({
	type,
	state
}) => {
	return (
		<div>
			{
				type == 'MATERIAL_BUDGET_EXCEL' ?
					<div>
				      	{
				      		state.MATERIAL_BUDGET_EXCEL ?
				      			<h4>Файл загружен</h4>
				      		:
				      			<div></div>
				      	}
					</div>
				: type == 'MATERIAL_BUDGET_PDF' ?
					<div>
				      	{
				      		state.MATERIAL_BUDGET_PDF ?
				      			<h4>Файл загружен</h4>
				      		:
				      			<div></div>
				      	}
					</div>
				: type == 'FLAT_ARRANGEMENT_AUTOCAD' ?
					<div>
				      	{
				      		state.FLAT_ARRANGEMENT_AUTOCAD ?
				      			<h4>Файл загружен</h4>
				      		:
				      			<div></div>
				      	}
					</div>
				: type == 'APARTMENT_MODEL_3DMAX' ?
					<div>
				      	{
				      		state.APARTMENT_MODEL_3DMAX ?
				      			<h4>Файл загружен</h4>
				      		:
				      			<div></div>
				      	}
					</div>
				: type == 'APARTMENT_DESIGN_REVIT' ?
					<div>
				      	{
				      		state.APARTMENT_DESIGN_REVIT ?
				      			<h4>Файл загружен</h4>
				      		:
				      			<div></div>
				      	}
					</div>
				: type == 'APARTMENT_DESIGN_PDF' ?
					<div>
				      	{
				      		state.APARTMENT_DESIGN_PDF ?
				      			<h4>Файл загружен</h4>
				      		:
				      			<div></div>
				      	}
					</div>
				:
					<div></div>
			}
		</div>
	)
}

export default CheckLoadedFile;