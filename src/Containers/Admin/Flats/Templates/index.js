import React, {
	useEffect,
	useRef,
	useState,
	useLayoutEffect
} from 'react';

import { 
	Header,
	RightSide,
	Skeleton
} from '../../../../Components';
import config from '../../../../Config';
import {
	TextField
} from '@material-ui/core';

//change icon 360 #4

import {
	useDispatch,
	useSelector
} from 'react-redux';

import { 
	AppBar,
	Tabs,
	Tab
} from '@material-ui/core';
import { Link } from 'react-router-dom';

import { 
	Delete,
	Edit,
	ThreeDRotation,
	AddBox,
	BusinessCenter
} from '@material-ui/icons';

import SimpleImageSlider from "react-simple-image-slider";

import { api } from '../../../../Services';
import { 
	Autocomplete
} from '@material-ui/lab';

const FlatTemplates = () => {

	const [page, setPage] = useState(1);
	const [complexApartment, setComplexApartment] = useState('');
	const [widthEl, setWidthEl] = useState(0);
	const dispatch = useDispatch();
	const getFlatsState = useSelector(state => state.flats);
	const getCacheState = useSelector(state => state.cache);
	const refItem = useRef(null);

	useEffect(()=>{

		if(getCacheState.saveComplexCache){
			dispatch(api.flats.getTemplates(1, getCacheState.saveComplexCache ? getCacheState.saveComplexCache._id : undefined));
		}else{
			dispatch(api.flats.getTemplates(page, complexApartment));
		}

		setTimeout(()=>{
		    if (refItem.current) {
		    	setWidthEl(refItem.current.offsetWidth);
		    }

		}, 1600)

	}, []);

	const deleteTemplate = (id) => {

		return dispatch(api.flats.deleteTemplate(id));

	}

	const getMoreTemplates = () => {

		if(getCacheState.saveComplexCache){
			dispatch(api.flats.getTemplates(page + 1, getCacheState.saveComplexCache ? getCacheState.saveComplexCache._id : undefined));
		}else{
			dispatch(api.flats.getTemplates(page + 1, complexApartment));
		}

		setPage(page + 1);
	}

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<content className="cities-page-block">

									<div className="header-page-main">
										<h1>шаблоны квартир</h1>
										<Link className="template-link-block" to={`/apartments/flats/templates/add`}>
											<span>Создать шаблон</span>
											<AddBox className="block-item-flat-button-template"/>
										</Link>
									</div>
									<div className="search-block-complex-templates">
										<Autocomplete
											renderOption={(option) => (
										        <React.Fragment>
										          	<h5 className="popper-option-style">{option.title}</h5>
										        </React.Fragment>
										    )}
											freeSolo
											autoHighlight={true}
											disablePortal
									        id="auto-select"
									        disableClearable={true}
									        underlineShow={false}
									        className="MuiAutocomplete-paper"
									        onChange={(event, value) => {
									        	setPage(1);
									        	setComplexApartment(value ? value._id : '');
									        	dispatch({
									        		type:'CLEAR_TEMPLATES'
									        	});
									        	dispatch({
													type:'UPDATE_CACHE_COMPLEX',
													cacheComplex:value
												});
									        	dispatch(api.flats.getTemplates(1, value ? value._id : undefined));
									        }}
									        style={{
									        	width:'100%',
									        	margin:'0 0 10px 0',
									        	padding:0
									        }}
									        autoComplete='false'
									        getOptionSelected={(option, value) => option.title === value.title}
									        options={getFlatsState.complexApartments}
									        defaultValue={getCacheState.saveComplexCache ? getCacheState.saveComplexCache : ''}
									        getOptionLabel={(option) => option.title}
									        renderInput={(params) => {
									        	return <TextField style={{
									        		width:'100%',
									        		margin:0,
									        		padding:'0',
									        		fontSize:18
									        	}}
									        	fullWidth
									        	placeholder="Выбрать ЖК"
									        	readonly  
									        	{...params}
									        	floatingLabelFocusStyle={{
													color: 'red'
									        	}}
									        	InputProps={{
									        		...params.InputProps,
      												autoComplete:'false',
									        		disableUnderline: true,
												    className:{
												    	input:{
												    		fontSize:22
												    	},
												    	InputLabelProps:{
														    style: { color: 'red' },
														}
												    }
									        	}} label="" />
									        }}
									    />
									</div>
									<div className="content-items-flat">
										{
											getFlatsState.request_flats_status ?
												<h3>Загружаем, подождите...</h3>
											: getFlatsState.templates && getFlatsState.templates.length ?
												getFlatsState.templates.map((template, index)=>{

													let defaultHomeUrl = template.preview_photo && template.preview_photo.url ? `${config.public}${template.preview_photo.url}` : require('../../../../assets/default-home.png');

													return (
														<div ref={refItem} className="block-item-flat">
															<section style={{
																width:'100%',
																height:170,
																background:`url(${defaultHomeUrl}) center / contain no-repeat`
															}}></section>
															<div className="block-item-flat-information">
																<p>{ template.title }</p>
																<h5><span>Кол-во комнат:</span> { template.count_rooms }</h5>
																<h5><span>ЖК:</span> { template.apartment_complex_id.title }</h5>
															</div>
															<div className="block-item-flat-information block-item-flat-buttons">
																<Delete onClick={()=>{deleteTemplate(template._id)}} className="block-item-flat-button-delete"/>
																<Link to={`/apartments/flats/${template._id}/edit`}>
																	<Edit className="block-item-flat-button-edit"/>
																</Link>
																<Link className="link-button-360" to={`/apartments/flats/${template._id}/360`}>
																	<ThreeDRotation className="icon-button-360" />
																</Link>
															</div>
														</div>
													)
												})
											:
												<h3>Шаблонов не найдено</h3>
										}
									</div>
									{
										getFlatsState.request_flats_status == false && getFlatsState.templates && getFlatsState.templates.length ?
											<span onClick={()=>{getMoreTemplates()}} className="more-flats">Показать больше { getFlatsState.templates ? `(Найдено: ${getFlatsState.templates.length})` : '' }</span>
										:
											<div></div>
									}
								</content>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default FlatTemplates;