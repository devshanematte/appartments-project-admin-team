import React, {
	useEffect,
	useState
} from 'react';
import { withRouter } from 'react-router-dom';
import { 
	Header,
	RightSide,
	Skeleton
} from '../../../../Components';
import { ReactSVG } from 'react-svg';
import { 
	useDispatch, 
	useSelector 
} from 'react-redux';
import { api } from '../../../../Services';
import config from '../../../../Config';

import { 
	AppBar,
	Tabs,
	Tab,
	TextField,
	Select,
	MenuItem,
	InputLabel,
	FormControl,
	Button,
	InputAdornment
} from '@material-ui/core';
import Dropzone from 'react-dropzone';

import {
	Delete
} from '@material-ui/icons';

import View360 from './Helpers/360';

const Flat360 = ({
	history,
	match
}) => {

	const dispatch = useDispatch();
	const getApartmentState = useSelector(state => state.apartments);
	const getFilesState = useSelector(state => state.files);

	useEffect(()=>{

		dispatch(api.apartments.get360ApartmentById(match.params.id));

	}, []);

	const uploadFile360 = (file, type) => {

		if(type == '360_PREVIEW'){
			return dispatch(api.files.uploadFile(file.target.files[0], type, getFilesState.photo_360.id));
		}
		return dispatch(api.files.uploadFile(file.target.files[0], type));

	}

	const addPhotos = () => {

		let {
			photo_360,
			photo_360_preview
		} = getFilesState;

		return dispatch(api.apartments.addPhotos360(photo_360, photo_360_preview, match.params.id));

	}

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">

								<h2>Квартира 360</h2>

								<div className="form-360-block">
									<section>
										<h5>Фотография комнаты формата 360</h5>
										<input onChange={(val)=>{ uploadFile360(val, '360') }} type="file"/>
										{
											getFilesState.photo_360 ?
												<span>Загружено</span>
											:
												<div></div>
										}
									</section>

									{
										getFilesState.photo_360 ?
											<section>
												<h5>Широкоформатная фотография комнаты</h5>
												<input onChange={(val)=>{ uploadFile360(val, '360_PREVIEW') }} type="file"/>
												{
													getFilesState.photo_360_preview ?
														<span>Загружено</span>
													:
														<div></div>
												}
											</section>
										:
											<div></div>
									}

									{
										getFilesState.request_upload_status ?
											<Button variant="contained" >Подождите, загружаем...</Button>
										:
											<Button variant="contained" onClick={()=>{addPhotos()}}>Добавить</Button>
									}
									
								</div>

								{
									getApartmentState.request_apartments_status ?
										<h1>Загрузка...</h1>
									: getApartmentState.apartment_360 && getApartmentState.apartment_360.photos.length > 0 ?
										<div className="block-image-360-with-dottes">
											<View360 template={getApartmentState.template} api={api} id={match.params.id} photos={getApartmentState.apartment_360} />
										</div>
									:
										<div>
											<h4>Информации нет. Добавьте материалы</h4>
										</div>
								}

							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default withRouter(Flat360);