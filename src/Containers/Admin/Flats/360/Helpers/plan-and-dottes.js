import React from 'react';
import config from '../../../../../Config';
import { 
	useDispatch
} from 'react-redux';

const PlanAndDottes = ({
	template,
	photo,
	api
}) => {

	const dispatch = useDispatch();

	let [left, setLeft] = React.useState(0);
	let [top, setTop] = React.useState(0);

	React.useEffect(()=>{

		setLeft(0);
		setTop(0);

	}, [photo])

	const eventPoint = async (event) => {

		let posXnative = event.nativeEvent.offsetX;
		let posYnative = event.nativeEvent.offsetY;
		let blockHeight = document.getElementById('plan-top-right-block').clientHeight;
		let blockwidth = document.getElementById('plan-top-right-block').clientWidth;

		let percentX = (posXnative/blockwidth) * 100;
		let percentY = (posYnative/blockHeight) * 100;

		setLeft(percentX);
		setTop(percentY);

		dispatch(api.apartments.saveDotForPlan(photo, percentX, percentY));

	}

	return (
		<div id="plan-top-right-block" className="plan-top-right-block" onClick={(e)=> eventPoint(e)} >
			<img src={`${config.public}${template.preview_photo.url}`} />

			{
				left > 0 && top > 0 ?
					<div className="dot-plan-block" style={{
						left:`${left}%`,
						top:`${top}%`
					}}></div>
				: photo.dott_for_room ?
					<div className="dot-plan-block" style={{
						left:`${photo.dott_for_room.left}%`,
						top:`${photo.dott_for_room.top}%`
					}}></div>
				:
					<div></div>
			}

		</div>
	)

}

export default PlanAndDottes;