import React, {
	useState,
	useRef
} from 'react';
import { Pannellum, PannellumVideo } from "pannellum-react";
import Config from '../../../../../Config';

import { 
	Delete
} from '@material-ui/icons';

import { 
	Button
} from '@material-ui/core';

import {
	useDispatch,
	useSelector
} from 'react-redux';

import PlanAndDottes from './plan-and-dottes';

import ItemScene from './item-scene';

const View360 = ({
	photos,
	id,
	api,
	template
}) => {

	const dispatch = useDispatch();

	const [indexPhoto, setIndexPhoto] = useState(0);

	const [pitchPhoto, setpitchPhoto] = useState(15);
	const [yawPhoto, setyawPhoto] = useState(160);
	const [buttonType, setButtonType] = useState('');
	const [modalStatusButton, setModalStatusButton] = useState(false);
	const [selectScene, setSelectScene] = useState(null);

	const [pointUrl, setPointUrl] = useState('');
	const [pointTitle, setPointTitle] = useState('');
	const [pointPrice, setPointPrice] = useState('');
	const [pointDiscount, setPointDiscount] = useState('');

	let refMain = useRef();

	const savePointPhoto = (type) => {
    	const getYaw = refMain.current.getViewer().getYaw();
    	const getPitch = refMain.current.getViewer().getPitch();
    	setpitchPhoto(getPitch);
    	setyawPhoto(getYaw);
    	setButtonType(type);
    	setModalStatusButton(true);
    	return

	}

	const delete360Photo = (_id) => {

		setIndexPhoto(0);
		return dispatch(api.apartments.remove360Photo(_id, id));
		
	}

	const renderPoints = (item, vrDataIndex) => {

		console.log(333, item)

		if(item.type == 'NEW_SCENE'){

			return <Pannellum.Hotspot
	            type="custom"
	            pitch={item.position.pitch}
	            yaw={item.position.yaw}
	            handleClick={(evt, args) => {
	            	if(photos.photos[indexPhoto].vr_data[vrDataIndex].indexScene >= 0){
	            		if( Number(photos.photos.length-1) >= item.indexScene){
	            			return setIndexPhoto(item.indexScene);
	            		}
	            	}
	            }}
	            handleClickArg={{ name: "test" }}
	        />
		} else {
	        return <Pannellum.Hotspot
	            type="info"
	            pitch={item.position.pitch}
	            yaw={item.position.yaw}
	            onClick={()=>{alert(2)}}
	            text={`${item.title} (${item.discount}), цена: ${item.price} тг.`}
	            URL={item.url}
	        >
	            <h2>{item.title}</h2>
	        </Pannellum.Hotspot>
		}

	}

	const addPointAndSave = () => {


		let {
			vr_data,
			_id
		} = photos.photos[indexPhoto];
		let newDataVr = {
			position: {
				pitch:pitchPhoto,
				yaw:yawPhoto
			},
			type:buttonType,
			indexScene:selectScene,
			url:'',
			title:'',
			price:''
		}

		const new_data_vr = [...vr_data, newDataVr];

		return dispatch(api.apartments.addPoint(new_data_vr, _id, id));

	}

	const addPointInformationAndSave = () => {

		if(pointUrl && pointTitle && pointPrice){

			let {
				vr_data,
				_id
			} = photos.photos[indexPhoto];
			let newDataVr = {
				position: {
					pitch:pitchPhoto,
					yaw:yawPhoto
				},
				type:buttonType,
				indexScene:selectScene,
				url:pointUrl,
				title:pointTitle,
				price:pointPrice,
				discount:pointDiscount
			}	

			const new_data_vr = [...vr_data, newDataVr];

			return dispatch(api.apartments.addPoint(new_data_vr, _id, id));

		}

		return alert('Заполните все поля');

	}

	const removePoint360 = (index) => {

		let {
			vr_data,
			_id
		} = photos.photos[indexPhoto];

		let removeVrData = [];
		vr_data.map((item, indexPoint)=>{
			
			if(indexPoint != index){
				removeVrData = [...removeVrData, item];
			}
		});

		const resRemovePoint = window.confirm('Точка будет удалена?');

		if(resRemovePoint){
			return dispatch(api.apartments.addPoint(removeVrData, _id, id));
		}

	}

	return (
		<div className="block-view-360">

			<div className="points-360-block">
				<h6>Список точек на сцене</h6>
		        {
		        	photos.photos[indexPhoto].vr_data && photos.photos[indexPhoto].vr_data.length > 0 ?

		        		photos.photos[indexPhoto].vr_data.map((item, index)=>{

		        		 	return (
		        		 		<div>
		        		 			{
		        		 				item.type == 'NEW_SCENE' && photos.photos[item.indexScene] ?
		        		 					<div className="item-section-point-360" key={index}>
		        		 						<h5>Точка с переходом на сцену</h5>
		        		 						<div className="photo-scene-block-preview" style={{
		        		 							background:`url(${Config.public}${photos.photos[item.indexScene].url}) center / cover no-repeat`
		        		 						}}></div>
		        		 						<span onClick={()=>{
								    				refMain.current.getViewer().setPitch(Number(item.position.pitch))
              										refMain.current.getViewer().setYaw(Number(item.position.yaw))
								    				return
		        		 						}}>Показать точку на сцене</span>
		        		 						<span className="remove-point-360" onClick={()=>{ removePoint360(index) }}>Удалить точку</span>
		        		 					</div>
		        		 				:
		        		 					<div className="item-section-point-360">
		        		 						<h5>Точка с информацией о продукте</h5>
		        		 						<div className="desc-scene-block-preview" >
		        		 							<p>Название: { item.title }</p>
		        		 							<p>Цена: { item.price }</p>
		        		 							<p>{ item.discount }</p>
		        		 						</div>
		        		 						<span onClick={()=>{
								    				refMain.current.getViewer().setPitch(Number(item.position.pitch))
              										refMain.current.getViewer().setYaw(Number(item.position.yaw))
								    				return
		        		 						}}>Показать точку на сцене</span>
		        		 						<span className="remove-point-360" onClick={()=>{ removePoint360(index) }}>Удалить точку</span>
		        		 					</div>
		        		 			}
		        		 		</div>
		        		 	)

		        		})

		        	:
		        		<h6>Не добавлено ни одной точки</h6>
		        }
			</div>

			<div className="modal-information-point">
				{
					modalStatusButton ?
						<section className="modal-information-point-form">
							<span><b>Тип точки:</b> { buttonType == 'NEW_SCENE' ? 'Новая сцена' : 'Описание продукта' }</span>

							{
								buttonType == 'NEW_SCENE' ?
									<div className="block-form-new-scene">
										<h5>Выберите сцену для перехода</h5>

										{
											photos.photos.map((scene, index)=>{

												return <ItemScene key={index} index={index} selectScene={(selectIndex)=>{ 
													setSelectScene(selectIndex);
													return
												}} scene={scene} />

											})
										}

										{
											selectScene || selectScene == 0 ?
												<div className="selected-scene-photo-block">
													<h4>Выбранная сцена</h4>
													<div className="selected-scene-photo" style={{
														background:`url(${Config.public}${photos.photos[selectScene].url}) center / cover no-repeat`
													}}></div>
													<br/>
													<Button variant="contained" onClick={()=>{ addPointAndSave() }}>Добавить сцену</Button>
												</div>
											:
												<div></div>
										}

									</div>
								:
									<div className="block-form-description-product">
										<h5>Заполните форму</h5>
										<input type="text" value={pointTitle} placeholder="Название продукта" onChange={(value)=>{setPointTitle(value.target.value)}} />
										<input type="text" value={pointUrl} placeholder="Ссылка" onChange={(value)=>{setPointUrl(value.target.value)}} />
										<input type="text" value={pointPrice} placeholder="Цена" onChange={(value)=>{setPointPrice(value.target.value)}} />
										<input type="text" value={pointDiscount} placeholder="Скидка" onChange={(value)=>{setPointDiscount(value.target.value)}} />
										<Button variant="contained" onClick={()=>{ addPointInformationAndSave() }}>Добавить информацию</Button>
									</div>
							}

						</section>
					:
						<div></div>
				}
			</div>

			<div className="block-view-360-content">

				{
					template && template.preview_photo && (
						<PlanAndDottes api={api} template={template} photo={photos.photos[indexPhoto]} />
					)
				}

		        <Pannellum
		            width="100%"
		            height="550px"
		            autoRotate={2.4}
		            image={`${Config.public}${photos.photos[indexPhoto].url}`}
		            ref={refMain}
		            pitch={3}
		            yaw={55}
		            hfov={100}
		            autoLoad
		            showControls={false}
		            onLoad={() => {
		                console.log("panorama loaded");
		            }}
		            hotspotDebug={true}
		        >

		        {
		        	photos.photos[indexPhoto].vr_data && photos.photos[indexPhoto].vr_data.length > 0 ?

		        		photos.photos[indexPhoto].vr_data.map((item, index)=>{

		        		 	return renderPoints(item, index);

		        		})

		        	:
		        		<div></div>
		        }

		        </Pannellum>

		        <span onClick={()=>{savePointPhoto('NEW_SCENE')}} className="button-new button-new-scene">Добавить новую сцену</span>
		        <span onClick={()=>{savePointPhoto('NEW_POINT')}} className="button-new button-new-point">Добавить описание</span>

	        </div>
	        <div className="controll-block-360">
	        	{
	        		photos.photos.map((item, index)=>{
	        			return (
	        				<span key={index} style={{
	        					background:`url(${Config.public}${item.url}) center / cover no-repeat`
	        				}}>
	        					<div onClick={()=>{
	        					setIndexPhoto(index);
	        				}} className="event-photo-select"></div>
		        				<div className="delete-360">
		        					<Delete onClick={()=>{delete360Photo(item._id)}} className="delete-360-icon" />
		        				</div>
	        				</span>
	        			)
	        		})
	        	}
	        </div>
		</div>
	)
}

export default View360;