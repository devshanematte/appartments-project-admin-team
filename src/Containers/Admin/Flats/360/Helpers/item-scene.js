import React from 'react';
import Config from '../../../../../Config';

const ItemScene = ({
	scene,
	selectScene,
	index
}) => {

	return (
		<div onClick={()=>{
			selectScene(index);
		}} className="scene-item-photo" style={{
			background:`url(${Config.public}${scene.url}) center / cover no-repeat`
		}}></div>
	)
}

export default ItemScene;