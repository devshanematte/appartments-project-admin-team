import React, {
	useEffect,
	useRef,
	useState,
	useLayoutEffect
} from 'react';

import { 
	Header,
	RightSide,
	Skeleton
} from '../../../Components';
import config from '../../../Config';

import {
	useDispatch,
	useSelector
} from 'react-redux';

import { 
	AppBar,
	Tabs,
	Tab
} from '@material-ui/core';
import { Link } from 'react-router-dom';

import { 
	Delete,
	Edit,
	ThreeDRotation,
	AddBox,
	BusinessCenter
} from '@material-ui/icons';

import SimpleImageSlider from "react-simple-image-slider";

import { api } from '../../../Services';

const Flats = () => {

	const [widthEl, setWidthEl] = useState(0);
	const [page, setPage] = useState(1);
	const dispatch = useDispatch();
	const getFlatsState = useSelector(state => state.flats);
	const refItem = useRef(null);

	useEffect(()=>{

		dispatch({
			type:'CLEAR_FLATS'
		});
		dispatch(api.flats.get(page));

		setTimeout(()=>{
		    if (refItem.current) {
		    	setWidthEl(refItem.current.offsetWidth);
		    }
		}, 1600)

	}, []);

	const renderSlider = (photos) => {

		let images = photos.map((image)=>{
			let url = config.public + image.url;
			return {
				url
			}
		})

		return <SimpleImageSlider
			width={widthEl}
            height={150}
            images={images}
            showNavs={false}
        />

	}

	const getMoreFlats = () => {
		dispatch(api.flats.get(page + 1));
		setPage(page + 1);
	}

	const deleteFlat = (id, field) => {

		return dispatch(api.flats.delete(id, field));

	}

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<content className="cities-page-block">

									<div className="header-page-main">
										<h1>Квартиры</h1>
										<Link className="template-link-block" to={`/apartments/flats/templates`}>
											<span>Шаблоны квартир</span>
											<BusinessCenter className="block-item-flat-button-template"/>
										</Link>
									</div>
									<div className="content-items-flat">
										{
											getFlatsState.request_flats_status ?
												<h3>Загружаем, подождите...</h3>
											: getFlatsState.list && getFlatsState.list.length ?
												getFlatsState.list.map((flat, index)=>{
													console.log(333, flat)
													let defaultHomeUrl = flat.template.preview_photo && flat.template.preview_photo.url ? `${config.public}${flat.template.preview_photo.url}` : require('../../../assets/default-home.png');

													return (
														<div ref={refItem} className="block-item-flat">
															<section style={{
																width:'100%',
																height:170,
																background:`url(${defaultHomeUrl}) center / cover no-repeat`
															}}></section>
															<div className="block-item-flat-information">
																<p>{ flat.title }</p>
																<h5><span>ЖК:</span> { flat.apartment_complex_id ? flat.apartment_complex_id.title : '-' }</h5>
																<h5><span>БЛОК:</span> { flat.block_id ? flat.block_id.title : '-' }</h5>
																<h5><span>Кол-во комнат:</span> { flat.template.count_rooms }</h5>
																<h5><span>Адрес:</span> { flat.apartment_complex_id ? flat.apartment_complex_id.address : '-' }</h5>
																<h5><span>Застройщик:</span> { flat.apartment_complex_id ? flat.apartment_complex_id.builder.title : '-' }</h5>
																<h5><span>Этаж:</span> { flat.floor }</h5>
															</div>
															<div className="block-item-flat-information block-item-flat-buttons">
																<Delete onClick={()=>{deleteFlat(flat._id, {
																	block_id:flat.block_id._id,
																	apartment_complex_id:flat.apartment_complex_id._id,
																	floor:flat.floor
																})}} className="block-item-flat-button-delete"/>
																<Link to={`/apartments/flats/${flat.template._id}/edit`}>
																	<Edit className="block-item-flat-button-edit"/>
																</Link>
															</div>
														</div>
													)
												})
											:
												<h3>Квартир не найдено</h3>
										}
									</div>
									{
										getFlatsState.request_flats_status == false && getFlatsState.list && getFlatsState.list.length ?
											<span onClick={()=>{getMoreFlats()}} className="more-flats">Показать больше</span>
										:
											<div></div>
									}
								</content>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default Flats;