import React from 'react';

import {
	Grid,
	ListItem,
	ListItemAvatar,
	Avatar,
	ListItemSecondaryAction,
	IconButton,
	List,
	ListItemText,
	Typography
} from '@material-ui/core';

import {
	Edit,
	Delete,
	LocationCity,
	Check
} from '@material-ui/icons';

import { useDispatch } from 'react-redux';

import { api } from '../../../Services';

const ItemCity = ({
	item,
	index
}) => {

	const dispatch = useDispatch();

	const [editCity, setEditCity] = React.useState(false);
	const [title, setTitle] = React.useState(item.title);

	const changeCityName = () => {

		setEditCity(!editCity);
		return dispatch(api.cities.changeCity(title, item._id, index));

	}

	const removeCity = () => {
		return
		return dispatch(api.cities.removeCity(item._id));

	}

	return (
		<section className="list-item-city">

			<Grid item xs={12} md={12} style={{padding:0}}>
	          <div>
	            <List dense={false} style={{padding:0}}>
	                <ListItem style={{padding:'10px 0'}}>
	                  <ListItemAvatar>
	                    <Avatar>
	                      <LocationCity />
	                    </Avatar>
	                  </ListItemAvatar>
	                  <ListItemText
	                    primary={editCity ? <input onChange={(val)=>{setTitle(val.target.value)}} className="list-item-city-edit-input" value={title} /> : <Typography type="body2" style={{ 
	                    	color: '#333',
	                    	fontSize:22
	                    }}>{title}</Typography>}
	                    secondary={<Typography type="body2" style={{ 
	                    	color: 'rgba(0,0,0,0.2)',
	                    	fontSize:13
	                    }}>{item.description}</Typography>}
	                  />
	                  <ListItemSecondaryAction>

	                  	{
	                  		editCity ?
			                    <IconButton onClick={()=>{changeCityName()}} edge="end" aria-label="delete">
			                      <Check />
			                    </IconButton>
			                :
			                    <IconButton onClick={()=>{setEditCity(!editCity)}} edge="end" aria-label="delete">
			                      <Edit />
			                    </IconButton> 
	                  	}

	                    <IconButton disabled onClick={()=>{removeCity()}} edge="end" aria-label="delete">
	                      <Delete />
	                    </IconButton>
	                  </ListItemSecondaryAction>
	                </ListItem>
	            </List>
	          </div>
	        </Grid>

		</section>
	)
}

export default ItemCity;