import React from 'react';

import { 
	Header,
	RightSide,
	Skeleton
} from '../../../Components';

import { 
	AppBar,
	Tabs,
	Tab
} from '@material-ui/core';

import { 
	useDispatch,
	useSelector
} from 'react-redux';

import { api } from '../../../Services';

import ItemCity from './itemCity';

const Cities = () => {

	const dispatch = useDispatch();
	const getCityState = useSelector(state => state.cities);

	React.useEffect(()=>{

		dispatch(api.cities.getCities());

	}, []);

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<content className="cities-page-block">
									<h1>Города</h1>

									{
										getCityState.request_city_status ?
											<Skeleton.List/>
										: getCityState.cities ?
											getCityState.cities.map((item, index)=>{
												return <ItemCity index={index} key={index} item={item} />
											})
										: 
											<h2>Городов нет</h2>
									}

								</content>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default Cities;