import React, {
	useEffect
} from 'react';
import { 
	Header,
	RightSide
} from '../../../Components';
import {
	useDispatch,
	useSelector
} from 'react-redux';

import { api } from '../../../Services/';
import moment from 'moment';
import {
	AccountCircle
} from '@material-ui/icons'

const Users = () => {

	const dispatch = useDispatch();
	const getUsers = useSelector(state => state.users);

	useEffect(()=>{
		dispatch(api.users.get());
	}, []);

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<div className="row">
									<content>
										<h1 className="title-page">Пользователи</h1>

										{
											getUsers.request_status_users ?
												<h4>Загружаем информацию</h4>
											: getUsers.list && getUsers.list.length ?
												<div className="block-users-items">
													{
														getUsers.list.map((user, index)=>{
															return (
																<div className="item-user" key={index}>
																	<div className="user-block-avatar">
																		<div className="user-block-avatar-overlay">
																			<AccountCircle style={{
																				fontSize:54,
																				color:'rgba(0,0,0,0.7)'
																			}}/>
																		</div>
																	</div>
																	<div className="user-information">
																		<h4>{ user.firstname }</h4>
																		<h5>{ user.email }</h5>
																		<span>{ user.role.type == 'USER' ? 'Пользователь' : 'Администратор' }</span>
																	</div>
																	<div className="user-date-regisration">
																		<h4>На сайте с {moment(user.createdAt).format('DD.MM.YYYY hh:mm')}</h4>
																	</div>
																</div>
															)
														})
													}
												</div>
											:
												<h5>Информации нет</h5>
										}
										
									</content>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default Users;