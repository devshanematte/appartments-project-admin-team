import React from 'react';
import { 
	Header,
	RightSide
} from '../../../../Components';

import ApartmentСomplex from './Helpers/ApartmentСomplex';

const CreateApartment = () => {

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<content className="page-create-apartment">
									<h1>Добавление нового жк</h1>
							      	<ApartmentСomplex/>
								</content>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default CreateApartment;