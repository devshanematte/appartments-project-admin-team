import React from 'react';
import { 
	Header,
	RightSide,
	Skeleton
} from '../../../Components';
import { Link } from 'react-router-dom';
import {
	GridList,
	GridListTile
} from '@material-ui/core';

import {
	Edit,
	Delete
} from '@material-ui/icons';

import {
	useSelector,
	useDispatch
} from 'react-redux';

import config from '../../../Config';
import randomInteger from '../../../Services/utils/random';

import { api } from '../../../Services';

import moment from 'moment';

const Apartments = () => {

	const dispatch = useDispatch();
	const getApartmentsState = useSelector(state => state.apartments);

	React.useEffect(()=>{

		dispatch(api.apartments.getApartmentsComplex());

	}, []);

	const deleteApartment = (id) => {
		let statusDeleteConfirm = window.confirm('Вы действительно хотите удалить ЖК? Восстановление будет невозможно');
		if(statusDeleteConfirm){
			return dispatch(api.apartments.deleteApartment(id));
		}
		return
	}

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<content className="page-apartment">
									<h1>Жилые комплексы</h1>

									{
										getApartmentsState.request_apartments_status ?
											<Skeleton.Blocks/>
										: getApartmentsState.apartments ?
									      	<GridList cellHeight={260} className='page-apartment-grid' cols={3}>
									        	{getApartmentsState.apartments.map((apartment, index) => {
									        		let urlPhoto = config.public + apartment.photo.url;
									        		return(
										          		<GridListTile key={index} cols={1 || 2}>
										          			<div className="apartment-block-content-item">
											          			<div className="apartment-image-block" style={{
											          				background:`url(${urlPhoto}) center / cover no-repeat`
											          			}}></div>
											          			<div className="apartment-block-content-item-overlay">
											          				<div className="apartment-block-content-item-overlay-top">
											          					<h2>{ apartment.title }</h2>
											          				</div>
											          				<div className="apartment-block-content-item-overlay-bottom">
											          					<small>{ moment(apartment.createdAt).format('Создано: DD/MM/YYYY в HH:mm') }</small>

											          					<section>
												          					<Link className="apartment-block-content-item-overlay-bottom-link-blocks" title="Редактировать" to={`/apartments/blocks/${apartment._id}`}>
												          						Управление секциями
												          					</Link>
											          					</section>

											          					<div className="apartment-block-content-item-overlay-settings">
											          						<Link title="Редактировать" to={`/apartments/edit/${apartment._id}`}>
											          							<Edit className="apartment-edit-icon"/>
											          						</Link>
											          						<span>
											          							<Delete onClick={()=>{deleteApartment(apartment._id)}} className="apartment-delete-icon"/>
											          						</span>
											          					</div>
											          				</div>
											          			</div>
											          		</div>
										          		</GridListTile>
									        		)
									        	})}
									      	</GridList>
										:
											<h2>Жилых комплексов не найдено</h2>
									}

								</content>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default Apartments;