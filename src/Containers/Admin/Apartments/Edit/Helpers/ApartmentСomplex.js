import React from 'react';
import { withRouter } from 'react-router-dom';
import { 
	AppBar,
	Tabs,
	Tab,
	TextField,
	Select,
	MenuItem,
	InputLabel,
	FormControl,
	Button
} from '@material-ui/core';
import Dropzone from 'react-dropzone';

import moment from 'moment';

import {
	Autorenew,
	Delete
} from '@material-ui/icons';

import { 
	YMaps, 
	Map,
	Placemark
} from 'react-yandex-maps';

import config from '../../../../../Config';

import { 
	useSelector,
	useDispatch
} from 'react-redux';

import { api } from '../../../../../Services';

const ApartmentСomplex = ({
	history,
	item
}) => {

	const dispatch = useDispatch();

	React.useEffect(()=>{

		dispatch(api.cities.getCities());
		dispatch(api.builders.getBuilders());

		dispatch({
			type:'APARTMENT_INTERIOR_ARRAY',
			photos: item.photos ? item.photos.map((photo)=>{
				return {
					url:photo.url,
					full_url:config.public + photo.url,
					id:photo._id
				}
			}) : null
		});


	}, []);

	console.log(333, item.delivery_time)

	let getCities = useSelector(state => state.cities.cities);
	let getBuilders = useSelector(state => state.builders.builders);
	const getApartmentsState = useSelector(state => state.apartments);
	const getFilesState = useSelector(state => state.files);

	const [deliveryTime, setDeliveryTime] = React.useState(item.delivery_time ? item.delivery_time : moment().format('YYYY-MM-DD'));
	const [min_price_square_metres, setMin_price_square_metres] = React.useState(item.min_price_square_metres ? item.min_price_square_metres : '');

	const [city, setCity] = React.useState(item.city._id);
	const [builder, setBuilder] = React.useState(item.builder._id);
	const [coords, setCoords] = React.useState([item.coords.lat, item.coords.lon]);
	const [title, setTitle] = React.useState(item.title);
	const [address, setAddress] = React.useState(item.address);
	const [housing_class, setHousing_class] = React.useState(item.housing_class);

  	const handleChangeCity = (event) => {
    	setCity(event.target.value);
  	};

  	const handleChangeBuilder = (event) => {
    	setBuilder(event.target.value);
  	};

  	const handleDrop = (files) => {
    	dispatch(api.files.uploadFile(files[0], 'complex-image'));
	}

	const handleDropBlocks = (files) => {
    	dispatch(api.files.uploadFile(files[0], 'images-complex-with-all-blocks'));
	}

  	const handleHousingClass = (event) => {
    	setHousing_class(event.target.value);
  	};	

	const editApartment = () => {

		let fields = {
			city,
			builder,
			coords,
			title,
			address,
			photo:getFilesState.photo_apartment_complex,
			housing_class,
			id:item._id,
			image_complex_with_all_blocks:getFilesState.photo_apartment_complex_with_blocks,
			photos:getFilesState.APARTMENT_INTERIOR,
			delivery_time:deliveryTime,
			min_price_square_metres
		};

		return dispatch(api.apartments.editApartment(fields, history));
	}

	return (
		<div className="ApartmentСomplex">

			<section>
				<small>{ moment(item.createdAt).format('Создано DD/MM/YYYY в HH:mm') }</small>
			</section>

			<section className="input-section-form" style={{marginTop:15}}>
	        	<TextField 
	        		fullWidth
	        		onChange={(val)=>{setTitle(val.target.value)}}
	        		id="standard-basic" 
	        		label="Название ЖК*"
	        		inputProps={{style: {
	        			fontSize: 16
	        		}}}
	        		value={title}
	  				InputLabelProps={{style: {fontSize: 16}}}
	        	/>
        	</section>

			<section className="input-section-form" style={{marginTop:15}}>
	        	<TextField 
	        		fullWidth
	        		onChange={(val)=>{setMin_price_square_metres(val.target.value)}}
	        		id="standard-basic" 
	        		label="Минимальная стоимость квадратного метра (тг)*"
	        		inputProps={{style: {
	        			fontSize: 16
	        		}}}
	        		type="number"
	        		value={min_price_square_metres}
	  				InputLabelProps={{style: {fontSize: 16}}}
	        	/>
        	</section>

        	<section className="input-section-form">
	        	<TextField 
	        		fullWidth
	        		type="text"
	        		onChange={(val)=>{setDeliveryTime(val.target.value)}}
	        		id="standard-basic" 
	        		label="Срок сдачи дома*"
	        		inputProps={{style: {
	        			fontSize: 16
	        		}}}
	        		value={deliveryTime}
	  				InputLabelProps={{style: {fontSize: 16}}}
	        	/>
        	</section>

        	<section className="input-section-form">
	        	<TextField 
	        		fullWidth
	        		onChange={(val)=>{setAddress(val.target.value)}}
	        		id="standard-basic" 
	        		label="Адрес ЖК*"
	        		inputProps={{style: {
	        			fontSize: 16
	        		}}}
	        		value={address}
	  				InputLabelProps={{style: {fontSize: 16}}}
	        	/>
        	</section>

        	<section className="input-section-form">
        		<FormControl className='select-option-style'>
	        		<InputLabel style={{fontSize:17}} htmlFor="age-native-simple">Выберите город*</InputLabel>
		        	<Select
		          		labelId="demo-simple-select-label"
		          		id="demo-simple-select"
		          		value={city}
		          		fullWidth
		          		onChange={handleChangeCity}
		          		style={{fontSize:17}}
		        	>
		        		{
		        			getCities ?
		        				getCities.map((item, index)=>{
		        					return <MenuItem style={{fontSize:17}} value={item._id}>{ item.title }</MenuItem>
		        				})
		        			:
		        				<MenuItem style={{fontSize:17}} value=''>Городов не найдено. Добавьте в разделе "Города"</MenuItem>
		        		}

		        	</Select>
	        	</FormControl>
        	</section>

        	<section className="input-section-form">
        		<FormControl className='select-option-style'>
	        		<InputLabel style={{fontSize:17}} htmlFor="age-native-simple">Выберите застройщика*</InputLabel>
		        	<Select
		          		labelId="demo-simple-select-label"
		          		id="demo-simple-select"
		          		value={builder}
		          		fullWidth
		          		onChange={handleChangeBuilder}
		          		style={{fontSize:17}}
		        	>
		        		{
		        			getBuilders ?
		        				getBuilders.map((item, index)=>{
		        					return <MenuItem style={{fontSize:17}} value={item._id}>{ item.title }</MenuItem>
		        				})
		        			:
		        				<MenuItem style={{fontSize:17}} value=''>Застройщиков не найдено. Добавьте в разделе "Застройщики"</MenuItem>
		        		}

		        	</Select>
	        	</FormControl>
        	</section>

        	<section className="input-section-form">
        		<FormControl className='select-option-style'>
	        		<InputLabel style={{fontSize:17}} htmlFor="age-native-simple">Выберите класс жилья*</InputLabel>
		        	<Select
		          		labelId="demo-simple-select-label"
		          		id="demo-simple-select"
		          		value={housing_class}
		          		fullWidth
		          		onChange={handleHousingClass}
		          		style={{fontSize:17}}
		        	>
		        		<MenuItem style={{fontSize:17}} value="">Выберите класс жилья</MenuItem>
		        		<MenuItem style={{fontSize:17}} value="STANDART">СТАНДАРТ</MenuItem>
		        		<MenuItem style={{fontSize:17}} value="COMFORT">КОМФОРТ</MenuItem>
		        		<MenuItem style={{fontSize:17}} value="BUSINESS">БИЗНЕС</MenuItem>
		        		<MenuItem style={{fontSize:17}} value="PREMIUM">ПРЕМИУМ</MenuItem>
		        	</Select>
	        	</FormControl>
        	</section>

        	<section className="drag-photo-complex-content">
        		
        		<h2>Фотография ЖК*</h2>

        		<div className="section-block-photos">

					<Dropzone onDrop={handleDrop.bind(undefined)}>
					  	{({getRootProps, getInputProps}) => (
						    <section className="drag-photo-complex">
						    	{
						    		getFilesState.request_upload_status ?
						    			<div>
						    				<Autorenew size={55} className="spin" />
						    			</div>
						    		:
								      	<div {...getRootProps()}>
								        	<input {...getInputProps()} />
								        	<p>Для загрузки фотографии можете перенести файл сюда или нажать на область</p>
								      	</div>
						    	}
						    </section>
					  	)}
					</Dropzone>

					{
						getFilesState.photo_apartment_complex ? 
							<div className="photo-apartment-url" style={{
								background:`url(${getFilesState.photo_apartment_complex.full_url}) center / cover no-repeat`
							}}></div>
						:
							<div></div>
					}

        		</div>

        	</section>

			<div className="plan-block-file">
				<h4>Интерьер квартиры</h4>
				{
					getFilesState.request_upload_status ?
						<h5>Подождите...</h5>
					:
						<input type="file" onChange={(val)=>{
							dispatch(api.files.uploadFile(val.target.files[0], 'IMAGE'));
						}}/>
				}
				<div className="images-block-interior-flat">
					{
						getFilesState.APARTMENT_INTERIOR ?
							getFilesState.APARTMENT_INTERIOR.map((item, index)=>{
								return <div key={index} className="image-interior-flat" style={{background:`url(${item.full_url}) center / cover no-repeat`}}>
									<span onClick={()=>{
										dispatch({
											type:'DELETE_IMAGE_APARTMENT_INTERIOR',
											index
										});
									}}><Delete/></span>
								</div>
							})
						:
							<div><p>Фотографий нет</p></div>
					}
				</div>
			</div>

        	<section className="drag-photo-complex-content">
        		
        		<h2>Панорамная фотография жк с блоками*</h2>

        		<div className="section-block-photos">

					<Dropzone onDrop={handleDropBlocks.bind(undefined)}>
					  	{({getRootProps, getInputProps}) => (
						    <section className="drag-photo-complex">
						    	{
						    		getFilesState.request_upload_status ?
						    			<div>
						    				<Autorenew size={55} className="spin" />
						    			</div>
						    		:
								      	<div {...getRootProps()}>
								        	<input {...getInputProps()} />
								        	<p>Для загрузки фотографии можете перенести файл сюда или нажать на область</p>
								      	</div>
						    	}
						    </section>
					  	)}
					</Dropzone>

					{
						getFilesState.photo_apartment_complex_with_blocks ? 
							<div className="photo-apartment-url" style={{
								background:`url(${getFilesState.photo_apartment_complex_with_blocks.full_url}) center / cover no-repeat`
							}}></div>
						:
							<div></div>
					}

        		</div>

        	</section>

        	<section style={{
        		marginBottom:20
        	}}>
        		
        		<h2>Отметьте местоположение ЖК на карте*</h2>

        		{
        			coords ?
        				<small>{ coords[0].toFixed(3) }, { coords[1].toFixed(3) }</small>
        			:
        				<div></div>
        		}

			  	<YMaps>
			      	<Map 
			      		defaultState={{ 
			      			center: [50.698, 70.191], 
			      			zoom: 4
			      		}}
			      		style={{
			      			width:'100%',
			      			height:500
			      		}}
			      		onClick={(e)=>{
			      			console.log(e.get("coords"))
			      			setCoords(e.get("coords"));
			      		}}
			      	>
		        		{
		        			coords ?
		        				<Placemark geometry={coords} />
		        			:
		        				<div></div>
		        		}
			      	</Map>
			  	</YMaps>

        	</section>

        	<section>
        		{
        			item.updatedAt ?
        				<small>{ moment(item.updatedAt).format('Последнее изменение DD/MM/YYYY в HH:mm') }</small>
        			:
        				<div></div>
        		}
        	</section>

        	<section>

				{
					getApartmentsState.request_apartments_status ?
						<Button variant="contained">Подождите</Button>
					:
						<Button variant="contained" onClick={()=>{editApartment()}}>Редактировать</Button>
				}        		
        	</section>

		</div>
	)
}

export default withRouter(ApartmentСomplex);