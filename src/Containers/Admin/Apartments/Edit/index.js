import React from 'react';
import { 
	Header,
	RightSide
} from '../../../../Components';
import { withRouter, Redirect } from 'react-router-dom';
import ApartmentСomplex from './Helpers/ApartmentСomplex';
import {
	useSelector,
	useDispatch
} from 'react-redux';
import { api } from '../../../../Services';
import moment from 'moment';

const EditApartment = ({match}) => {
	
	const { id } = match.params;

	const dispatch = useDispatch();
	const getApartmentState = useSelector(state => state.apartments);

	React.useEffect(()=>{

		dispatch(api.apartments.getApartmentById(id));

	}, []);

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<content className="page-create-apartment">
									<h1>Редактирование жк</h1>
									
									{
										getApartmentState.request_apartments_status ?
											<div>
												<h3>Подождите...</h3>
											</div>
										: getApartmentState.getApartmentForEdit ?
											<ApartmentСomplex item={getApartmentState.getApartmentForEdit}/>
										:
											<div></div>
									}
								</content>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default withRouter(EditApartment);