import React, {
	useEffect
} from 'react';
import { 
	Header,
	RightSide,
	Skeleton
} from '../../../Components';

import {
	useDispatch,
	useSelector
} from 'react-redux';

import { api } from '../../../Services';

import moment from 'moment';

const Admin = () => {

	const dispatch = useDispatch();
	const getStateStatistic = useSelector(state => state.statistic);

	useEffect(()=>{

		dispatch(api.statistic.get());

	}, []);

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9 statistic-block">
								<h4>Информация актуальна на <span>{moment(new Date()).format('DD.MM.YYYY')}</span></h4>
								{
									getStateStatistic.request_status_statistic ?
										<h5>Обновляем информацию</h5>
									: getStateStatistic.information ?
										<content>
											<section>
												<h1>Всего квартир</h1>
												<h5>{ getStateStatistic.information.flats }</h5>
											</section>
											<section>
												<h1>Всего шаблонов</h1>
												<h5>{ getStateStatistic.information.templates }</h5>
											</section>
											<section>
												<h1>Зарегистрировано пользователей</h1>
												<h5>{ getStateStatistic.information.users }</h5>
											</section>
											<section>
												<h1>Всего блоков</h1>
												<h5>{ getStateStatistic.information.blocks }</h5>
											</section>
											<section>
												<h1>Всего жилых комплексов</h1>
												<h5>{ getStateStatistic.information.apartmentsComplex }</h5>
											</section>
											<section>
												<h1>Загружено файлов</h1>
												<h5>{ getStateStatistic.information.files }</h5>
											</section>
										</content>
									:
										<h5>Информации нет</h5>
								}
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default Admin;