import React, {
	useEffect
} from 'react';
import { 
	Header,
	RightSide,
	Skeleton
} from '../../../Components';

import {
	useDispatch,
	useSelector
} from 'react-redux';

import { api } from '../../../Services';

import moment from 'moment';

import Prices from './Helpers/prices';

const Settings = () => {

	const dispatch = useDispatch();

	useEffect(()=>{

		dispatch(api.statistic.get());
		dispatch(api.statistic.settings());

	}, []);

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9 statistic-block">
								<h1>Настройки</h1>
								
								<Prices api={api}/>

							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default Settings;