import React, {
	useState,
	useEffect
} from 'react';

import {
	useDispatch,
	useSelector
} from 'react-redux';

import _ from 'underscore';

import { 
	Button
} from '@material-ui/core';

const Prices = ({
	api
}) => {

	const [status, setStatus] = useState(false);
	const [standart, setStandart] = useState('');
	const [business, setBusiness] = useState('');
	const [comfort, setComfort] = useState('');
	const [premium, setPremium] = useState('');
	const dispatch = useDispatch();

	const getSettings = useSelector(state => state.statistic.settings);

	useEffect(()=>{

		updatePrices();

	}, [getSettings]);

	const updatePrices = () => {
		if(getSettings && getSettings.length){
			getSettings.map((item)=>{
			   	if(item.type == 'COMFORT'){
			   		setComfort(item.value);
			   		return;
			   	}else if(item.type == 'BUSINESS'){
			   		setBusiness(item.value);
			   		return;
			   	}else if(item.type == 'PREMIUM'){
			   		setPremium(item.value);
			   		return;
			   	}else if(item.type == 'STANDART'){
			   		setStandart(item.value);
			   		return;
			   	};
			})
		}
	}

	const saveSettingPrices = () => {

		let fileds = {
			standart,
			business,
			comfort,
			premium
		};

		return dispatch(api.statistic.saveSettingPrices(fileds));
		
	}

	return (
		<div className="section-settings-block">
			<div className="section-settings-block-head">
				<h3>Средние цены на жилье</h3>
				<span onClick={()=>{setStatus(!status)}}>{ status ? 'Скрыть' : 'Показать' }</span>
			</div>

			{
				status ?
					<div className="section-settings-block-body">
						<div className="setting-block">
							<p>Стандарт</p>
							<div className="setting-block-input">
								<input onChange={(val)=>{setStandart(val.target.value)}} type="text" value={standart}/>
								<h5>тг.</h5>
							</div>
						</div>
						<div className="setting-block">
							<p>Комфорт</p>
							<div className="setting-block-input">
								<input onChange={(val)=>{setComfort(val.target.value)}} type="text" value={comfort}/>
								<h5>тг.</h5>
							</div>
						</div>
						<div className="setting-block">
							<p>Бизнес</p>
							<div className="setting-block-input">
								<input onChange={(val)=>{setBusiness(val.target.value)}} type="text" value={business}/>
								<h5>тг.</h5>
							</div>
						</div>
						<div className="setting-block">
							<p>Премиум</p>
							<div className="setting-block-input">
								<input onChange={(val)=>{setPremium(val.target.value)}} type="text" value={premium}/>
								<h5>тг.</h5>
							</div>
						</div>
						<Button variant="contained" onClick={saveSettingPrices.bind(undefined)}>Сохранить</Button>
					</div>
				:
					<div></div>
			}

		</div>
	)

}

export default Prices;