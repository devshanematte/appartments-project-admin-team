import React from 'react';
import { withRouter } from 'react-router-dom';
import { 
	Header,
	RightSide,
	Skeleton
} from '../../../../Components';

import { 
	TextField,
	Button
} from '@material-ui/core';

import { 
	useDispatch, 
	useSelector 
} from 'react-redux';
import { api } from '../../../../Services';

const CreateBuilder = ({
	history
}) => {

	const dispatch = useDispatch();
	const getBuildersState = useSelector(state => state.builders);
	const getFiles = useSelector(state => state.files);

	const [title, setTitle] = React.useState('');
	const [description, setDescription] = React.useState('');
	const [phone, setPhone] = React.useState('');
	const [year, setYear] = React.useState('');

	const addBuilder = () => {

		return dispatch(api.builders.addBuilder(title, description, history, phone, year, getFiles.logo_builder ));

	}

	const uploadLogo = (file) => {
		dispatch(api.files.uploadFile(file, 'LOGO_BUILDER'));
		return
	}

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<content className="cities-page-block">
									<h1>Добавление застройщика</h1>

									<section className="input-section-form">
							        	<TextField 
							        		fullWidth
							        		id="standard-basic" 
							        		label="Название"
							        		inputProps={{style: {
							        			fontSize: 16
							        		}}}
							  				InputLabelProps={{style: {fontSize: 16}}}
							  				onChange={(val)=>{setTitle(val.target.value)}}
							        	/>
        							</section>

									<section className="input-section-form">
							        	<TextField 
							        		fullWidth
							        		id="standard-basic" 
							        		label="Описание (не обязательно)"
							        		multiline
											rows={2}
											rowsMax={4}
							        		inputProps={{style: {
							        			fontSize: 16
							        		}}}
							  				InputLabelProps={{style: {fontSize: 16}}}
							  				onChange={(val)=>{setDescription(val.target.value)}}
							        	/>
        							</section>

									<section className="input-section-form">
							        	<TextField 
							        		fullWidth
							        		id="standard-basic" 
							        		label="Телефон"
							        		inputProps={{style: {
							        			fontSize: 16
							        		}}}
							  				InputLabelProps={{style: {fontSize: 16}}}
							  				onChange={(val)=>{setPhone(val.target.value)}}
							        	/>
        							</section>

									<section className="input-section-form">
							        	<TextField 
							        		fullWidth
							        		id="standard-basic" 
							        		label="Год основания"
							        		inputProps={{style: {
							        			fontSize: 16
							        		}}}
							  				InputLabelProps={{style: {fontSize: 16}}}
							  				onChange={(val)=>{setYear(val.target.value)}}
							        	/>
        							</section>

									<section className="input-section-form">
							        	<input
							        		type="file"
							        		onChange={(val)=>uploadLogo(val.target.files[0])}
							        	/>
        							</section>

        							{
        								getFiles.request_upload_status ?
        									<h4>Загружаем...</h4>
        								: getFiles.logo_builder ?
        									<div className="logo-builder-preview" style={{
        										background:`url(${getFiles.logo_builder.full_url}) center / cover no-repeat`
        									}}></div>
        								:
        									<div><p>Логотип не найден</p></div>	
        							}

        							{
        								getBuildersState.request_builder_status ?
        									<Button variant="contained">Подождите</Button>
        								:
        									<Button variant="contained" onClick={()=>{addBuilder()}}>Добавить</Button>
        							}

								</content>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default withRouter(CreateBuilder);