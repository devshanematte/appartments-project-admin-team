import React from 'react';

import {
	Grid,
	ListItem,
	ListItemAvatar,
	Avatar,
	ListItemSecondaryAction,
	IconButton,
	List,
	ListItemText,
	Typography,
} from '@material-ui/core';

import {
	Edit,
	Delete,
	LocationCity,
	Check,
	BorderColor
} from '@material-ui/icons';

import { useDispatch, useSelector } from 'react-redux';

import { api } from '../../../Services';

import config from '../.././../Config';

const ItemBuilder = ({
	item,
	index
}) => {

	const dispatch = useDispatch();

	const getFiles = useSelector(state => state.files);

	const [editTitle, setEditTitle] = React.useState(false);
	const [title, setTitle] = React.useState(item.title);
	const [phone, setPhone] = React.useState(item.phone ? item.phone : '');
	const [year, setYear] = React.useState(item.year ? item.year : '');
	const [logo, setLogo] = React.useState(item.logo ? item.logo : '');
	const [desc, setDesc] = React.useState(item.description ? item.description : '');

	const changeCityName = () => {

		setEditTitle(!editTitle);

		let logoId = getFiles.logo_builder ? getFiles.logo_builder.id : logo._id;

		return dispatch(api.builders.changeBuilder(title, item._id, index, phone, desc, year, logoId));

	}

	const removeCity = () => {
		return
		return dispatch(api.builders.removeBuilder(item._id));

	}

	const uploadLogo = (file) => {
		dispatch(api.files.uploadFile(file, 'LOGO_BUILDER'));
		return
	}

	return (
		<section className="list-item-city">

			<Grid item xs={12} md={12} style={{padding:0}}>
	          <div>
	            <List dense={false} style={{padding:0}}>
	                <ListItem style={{padding:'10px 0'}}>
	                  <ListItemAvatar>
	                    <Avatar>
	                      <BorderColor />
	                    </Avatar>
	                  </ListItemAvatar>
	                  <ListItemText
	                    primary={editTitle ? 
	                    	<div style={{
	                    		display:'flex',
	                    		flexDirection:'column'
	                    	}}>
	                    		<input onChange={(val)=>{setTitle(val.target.value)}} className="list-item-city-edit-input" value={title} />
	                    		<input placeholder="Телефон" onChange={(val)=>{setPhone(val.target.value)}} className="list-item-city-edit-input" value={phone} />
	                    		<input placeholder="Год основания" onChange={(val)=>{setYear(val.target.value)}} className="list-item-city-edit-input" value={year} />
	                    		<textarea className="textarea-desc-builder" onChange={(val)=>{setDesc(val.target.value)}}>{ desc }</textarea>

	                    		<input type="file" onChange={(val)=>uploadLogo(val.target.files[0])} />

								{
    								getFiles.request_upload_status ?
    									<h4>Загружаем...</h4>
    								: getFiles.logo_builder ?
    									<div className="logo-builder-preview" style={{
    										background:`url(${getFiles.logo_builder.full_url}) center / cover no-repeat`
    									}}></div>
    								: logo ?
    									<div className="logo-builder-preview" style={{
    										background:`url(${config.public + 'preview/' + logo.url}) center / cover no-repeat`
    									}}></div>
    								:
    									<div><p>Логотип не найден</p></div>	
    							}

	                    	</div>
	                    : <Typography type="body2" style={{ 
	                    	color: '#333',
	                    	fontSize:22
	                    }}>{title} { phone ? `(${phone})` : '' }</Typography>}
	                    
	                  />
	                  <ListItemSecondaryAction>

	                  	{
	                  		editTitle ?
			                    <IconButton onClick={()=>{changeCityName()}} edge="end" aria-label="delete">
			                      <Check />
			                    </IconButton>
			                :
			                    <IconButton onClick={()=>{setEditTitle(!editTitle)}} edge="end" aria-label="delete">
			                      <Edit />
			                    </IconButton> 
	                  	}

	                    <IconButton disabled onClick={()=>{removeCity()}} edge="end" aria-label="delete">
	                      <Delete />
	                    </IconButton>
	                  </ListItemSecondaryAction>
	                </ListItem>
	            </List>
	          </div>
	        </Grid>

		</section>
	)
}

export default ItemBuilder;