import React from 'react';

import { 
	Header,
	RightSide,
	Skeleton
} from '../../../Components';

import { 
	AppBar,
	Tabs,
	Tab
} from '@material-ui/core';
import {
	useSelector,
	useDispatch
} from 'react-redux';

import ItemBuilder from './itemBuilder';

import { api } from '../../../Services';

const Builders = () => {

	const dispatch = useDispatch();
	const getBuilderState = useSelector(state => state.builders);

	React.useEffect(()=>{

		dispatch(api.builders.getBuilders());

	}, []);

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<content className="cities-page-block">
									<h1>Застройщики</h1>

									{
										getBuilderState.request_builder_status ?
											<Skeleton.List/>
										: getBuilderState.builders ?
											getBuilderState.builders.map((item, index)=>{
												return <ItemBuilder index={index} key={index} item={item} />
											})
										: 
											<h2>Застройщиков нет</h2>
									}

								</content>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default Builders;