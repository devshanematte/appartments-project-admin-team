import React, {
	useEffect
} from 'react';
import { 
	Header,
	RightSide
} from '../../../Components';
import {
	useDispatch,
	useSelector
} from 'react-redux';

import { Link } from 'react-router-dom';

import { 
	Delete,
	Edit,
	AccountCircle
} from '@material-ui/icons';

import { api } from '../../../Services/';
import moment from 'moment';

const Pages = () => {

	const dispatch = useDispatch();
	const getPages = useSelector(state => state.pages);

	useEffect(()=>{

		dispatch({
			type:'UPDATE_PAGE_BY_ID',
			data:null
		});
		
		dispatch(api.pages.get());
	}, []);

	const removePage = (id) => {
		dispatch(api.pages.delete(id));
	}

	return (
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<div className="row">
									<content>
										<h1 className="title-page">Страницы</h1>

										{
											getPages.request_status ?
												<h4>Загружаем информацию</h4>
											: getPages.list && getPages.list.length ?
												<div className="block-users-items">
													{
														getPages.list.map((item, index)=>{

															let div = document.createElement('div');
															div.innerHTML = item.content;
															let firstImage = div.getElementsByTagName('img')[0]
															let imgSrc = firstImage ? firstImage.src : "";

															return (
																<div className="item-page" key={index} style={{
																	background:`url(${imgSrc}) center / cover no-repeat`
																}}>
																	<h1>{ item.title }</h1>
																	<div className="bottom-page-controll-block">
																		<span>Создано: <b>{ moment(item.createdAt).format('DD.MM.YYYY в hh:mm') }</b></span>
																		<div className="bottoms-page-controll-block">
																			<Delete onClick={()=>removePage(item._id)} className="bottom-page-controll-block-icon bottom-page-controll-block-icon-remove"/>
																			<Link to={`/pages/edit/${item._id}`}>
																				<Edit className="bottom-page-controll-block-icon bottom-page-controll-block-icon-edit"/>
																			</Link>
																		</div>
																	</div>
																</div>
															)
														})
													}
												</div>
											:
												<h5>Информации нет</h5>
										}
										
									</content>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	)
}

export default Pages;