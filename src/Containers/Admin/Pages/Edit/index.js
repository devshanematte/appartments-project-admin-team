import React, {
	useEffect,
	useRef,
	useState,
	useMemo
} from 'react';
import { 
	Header,
	RightSide
} from '../../../../Components';

import { withRouter } from 'react-router-dom';

import {
	useDispatch,
	useSelector
} from 'react-redux';

import { api } from '../../../../Services/';
import moment from 'moment';
import {
	AccountCircle
} from '@material-ui/icons';

import EditPageForm from './Helpers/form';

const EditPage = ({
	history,
	match
}) => {

	const dispatch = useDispatch();

	const getBuilder = useSelector(state => state.builders);
	const getPages = useSelector(state => state.pages);

	useEffect(()=>{

		dispatch(api.pages.getById(match.params.id));
		dispatch(api.builders.getBuilders());
		return

	}, [])

	return useMemo(()=>(
		<div className="app-main">

			<div className="container">
				<div className="app-admin-content">

					<Header/>

					<div className="col-md-12">
						<div className="row">
							<div className="col-md-3">
								<RightSide/>
							</div>
							<div className="col-md-9">
								<div className="row">
									<content className="page-editor-block">
										<h1 className="title-page">Создание страницы</h1>

										{
											getPages.request_status ?
												<h4>Загружаем информацию</h4>
											:
												<EditPageForm api={api} id={match.params.id} builders={getBuilder.builders} history={history} dispatch={dispatch} item={getPages.page_by_id} />
										}
									</content>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>	

		</div>
	), [getBuilder, getPages])
}

export default withRouter(EditPage);