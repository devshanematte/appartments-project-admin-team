import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { NotificationContainer } from 'react-notifications';

import 'react-notifications/lib/notifications.css';

import Admin from '../Admin/Home';
import Auth from '../Auth/Login';

import Apartments from '../Admin/Apartments';
import CreateApartment from '../Admin/Apartments/Create';
import EditApartment from '../Admin/Apartments/Edit';

import Users from '../Admin/Users';
import Cities from '../Admin/Cities';
import CreateCity from '../Admin/Cities/Create';
import Builders from '../Admin/Builders';
import CreateBuilder from '../Admin/Builders/Create';
import CreateBlocks from '../Admin/Blocks/Create';
import Flats from '../Admin/Flats';
import CreateFlat from '../Admin/Flats/Create';
import EditFlat from '../Admin/Flats/Edit';
import Flat360 from '../Admin/Flats/360';
import Settings from '../Admin/Settings';
import AddFloorsAndFlats from '../Admin/Blocks/Floors';
import FlatTemplates from '../Admin/Flats/Templates';
import Pages from '../Admin/Pages';
import CreatePage from '../Admin/Pages/Create';
import EditPage from '../Admin/Pages/Edit';

const Initial = () => {

	let getAuthInfo = useSelector((state)=>{
		return state.auth;
	})

	return (

		<div className="app-main">
			<Switch>
				<Route exact path='/' render={()=> getAuthInfo.logedIn ? <Admin/> : <Auth />}/>

				<Route exact path='/apartments' render={()=> getAuthInfo.logedIn ? <Apartments/> : <Redirect to="/" />}/>
				<Route exact path='/apartments/create' render={()=> getAuthInfo.logedIn ? <CreateApartment/> : <Redirect to="/" />}/>
				<Route exact path='/apartments/edit/:id' render={()=> getAuthInfo.logedIn ? <EditApartment/> : <Redirect to="/" />}/>
				<Route exact path='/apartments/blocks/:id' render={()=> getAuthInfo.logedIn ? <CreateBlocks/> : <Redirect to="/" />}/>
				<Route exact path='/apartments/blocks/:id/add-floors/:complex_id' render={()=> getAuthInfo.logedIn ? <AddFloorsAndFlats/> : <Redirect to="/" />}/>

				<Route exact path='/apartments/flats' render={()=> getAuthInfo.logedIn ? <Flats/> : <Redirect to="/" />}/>
				<Route exact path='/apartments/flats/templates' render={()=> getAuthInfo.logedIn ? <FlatTemplates/> : <Redirect to="/" />}/>
				<Route exact path='/apartments/flats/templates/add' render={()=> getAuthInfo.logedIn ? <CreateFlat/> : <Redirect to="/" />}/>
				<Route exact path='/apartments/flats/:id/edit' render={()=> getAuthInfo.logedIn ? <EditFlat/> : <Redirect to="/" />}/>
				<Route exact path='/apartments/flats/:id/360' render={()=> getAuthInfo.logedIn ? <Flat360/> : <Redirect to="/" />}/>

				<Route exact path='/users' render={()=> getAuthInfo.logedIn ? <Users/> : <Redirect to="/" />}/>

				<Route exact path='/pages' render={()=> getAuthInfo.logedIn ? <Pages/> : <Redirect to="/" />}/>
				<Route exact path='/pages/create' render={()=> getAuthInfo.logedIn ? <CreatePage/> : <Redirect to="/" />}/>
				<Route exact path='/pages/edit/:id' render={()=> getAuthInfo.logedIn ? <EditPage/> : <Redirect to="/" />}/>

				<Route exact path='/cities' render={()=> getAuthInfo.logedIn ? <Cities/> : <Redirect to="/" />}/>
				<Route exact path='/cities/create' render={()=> getAuthInfo.logedIn ? <CreateCity/> : <Redirect to="/" />}/>

				<Route exact path='/builders' render={()=> getAuthInfo.logedIn ? <Builders/> : <Redirect to="/" />}/>
				<Route exact path='/builders/create' render={()=> getAuthInfo.logedIn ? <CreateBuilder/> : <Redirect to="/" />}/>

				<Route exact path='/settings' render={()=> getAuthInfo.logedIn ? <Settings/> : <Redirect to="/" />}/>

				{/*Redirect if 404*/}
				<Route path='*' render={()=> <Redirect to="/" />}/>

		  	</Switch>

		  	<NotificationContainer/>

		</div>

	)
}

export default Initial;