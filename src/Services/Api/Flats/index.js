import axios, { updateAxiosConfig } from '../axios';
import { SessionToken } from '../../utils/session';
import authUser from "./index";
import { NotificationManager } from 'react-notifications';
import config from '../../../Config';
import blocks from '../Blocks';

const apiblocks = blocks;

const flats = {
	get: (page = 1) => async (dispatch) => {

		dispatch({
			type:'REQUEST_FLATS_STATUS',
			status:true
		});

		try{

			let { data } = await axios.get(`/admin/apartments?page=${page}`);

			dispatch({
				type:'UPDATE_FLATS',
				data:data.length ? data : null
			});

			setTimeout(()=>{
				dispatch({
					type:'REQUEST_FLATS_STATUS',
					status:false
				});
			}, 1000)

			return

		}catch(err){

			dispatch({
				type:'REQUEST_FLATS_STATUS',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}

	},
	updateFlatAreaPoints: (flat_id,EditBlockAreas,EditAreaPoints) => async dispatch => {

		try{

			let { data } = await axios.post('/admin/apartments/flat/update-flat-area-points', {
				id:flat_id,
				blockArea:EditBlockAreas,
				areaPointsString:EditAreaPoints
			});

			NotificationManager.success(data);
			return			

		}catch(err){
			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return
		}

	},
	getTemplates: (page, id, statusPage) => async (dispatch) => {

		dispatch({
			type:'REQUEST_FLATS_STATUS',
			status:true
		});

		id = id == '' ? undefined : id;

		try{

			let { data } = await axios.get(`/admin/apartments/flats/templates/${id}?page=${page}&statusPage=${statusPage}`);
			let complexApartments = await axios.get(`/admin/complex-apartments`);

			if(complexApartments.data && complexApartments.data.length){
				dispatch({
					type:'UPDATE_COMPLEX_APARTMENTS_FOR_TEMPLATES',
					data:complexApartments.data.length ? complexApartments.data : null
				});
			} else {
				NotificationManager.warning('Записей не найдено');
			}

			if(data.length){
				dispatch({
					type:'UPDATE_TEMPLATES',
					data:data.length ? data : null
				});
			}else {
				NotificationManager.warning('Записей не найдено');
			}

			setTimeout(()=>{
				dispatch({
					type:'REQUEST_FLATS_STATUS',
					status:false
				});
			}, 1000)

			return

		}catch(err){

			dispatch({
				type:'REQUEST_FLATS_STATUS',
				status:false
			});

			NotificationManager.error('Ошибка. Проверьте Ваши права администратора');
			return

		}

	},
	editTitle: (title, id) => async dispatch => {

		dispatch({
			type:'REQUEST_FLATS_STATUS',
			status:true
		});

		try {

			let { data } = await axios.post('/admin/apartments/flats/update/title', {
				title,
				id
			});

			dispatch({
				type:'REQUEST_FLATS_STATUS',
				status:false
			});

			NotificationManager.success('Название квартиры успешно изменено');
			return

		}catch(err){
			dispatch({
				type:'REQUEST_FLATS_STATUS',
				status:false
			});
			return
		}

	},
	deleteTemplate: (id) => async dispatch => {

		dispatch({
			type:'REQUEST_FLATS_STATUS',
			status:true
		});

		try {

			let { data } = await axios.post('/admin/apartments/flats/templates/remove' , {
				id
			});

			dispatch({
				type:'UPDATE_TEMPLATES',
				data:data.length ? data : null
			});

			dispatch({
				type:'REQUEST_FLATS_STATUS',
				status:false
			});

			return

		}catch(err){

			dispatch({
				type:'REQUEST_FLATS_STATUS',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}

	},
	copyFloor: (fields) => async dispatch => {

		let {
			block_id,
			apartment_complex_id,
			floor,
			copyFloor
		} = fields;

		dispatch({
			type:'REQUEST_FLATS_STATUS',
			status:true
		});

		try {

			let getFloor = await apiblocks.getFloorByBlockNotDispatch(block_id, apartment_complex_id, copyFloor);

			if(getFloor.flats && getFloor.flats.length >= 2){

				let { data } = await axios.post('/admin/apartments/floors/copy', {
					...fields,
					flats:getFloor.flats
				});

				dispatch(apiblocks.getFloorByBlock(block_id, apartment_complex_id, floor));

				return NotificationManager.success(data);

			}else {

				dispatch({
					type:'REQUEST_FLATS_STATUS',
					status:false
				});

				return NotificationManager.error('Квартир не было найдено на этом этаже, либо их слишком мало');

			}


		}catch(err){

			dispatch({
				type:'REQUEST_FLATS_STATUS',
				status:false
			});

			return

		}

	},
	create: (fields) => async dispatch => {

		let {
			title,
			square_metres,
			count_rooms,
			material_price,
			total_price,
			square_metre_price,
			files,
			plan_photo,
			block_id,
			checkFiles,
			ceiling_height,
			apartmentComplex
		} = fields;

		if(checkFiles.MATERIAL_BUDGET_EXCEL && checkFiles.MATERIAL_BUDGET_PDF && checkFiles.FLAT_ARRANGEMENT_AUTOCAD && checkFiles.APARTMENT_MODEL_3DMAX && checkFiles.APARTMENT_DESIGN_REVIT && checkFiles.APARTMENT_DESIGN_PDF){

			if(apartmentComplex && apartmentComplex != 'none' && title && ceiling_height && square_metres && count_rooms && material_price && total_price && square_metre_price && files && plan_photo){

				dispatch({
					type:'REQUEST_FLATS_STATUS',
					status:true
				});

				try{

					let { data } = await axios.post('/admin/apartments/create', {
						...fields,
						plan_photo:plan_photo.id
					});

					dispatch({
						type:'REQUEST_FLATS_STATUS',
						status:false
					});

					NotificationManager.success('Квартира успешно создана');
					return {
						status:true
					}

				}catch(err){
					dispatch({
						type:'REQUEST_FLATS_STATUS',
						status:false
					});

					NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
					return {
						status:false
					}
				}

			}

			NotificationManager.error('Заполните все поля!');
			return {
				status:false
			}

		}

		NotificationManager.error('Загрузите необходимые файлы!');
		return {
			status:false
		}

	},
	createwithPoints: (fields) => async dispatch => {

		dispatch({
			type:'REQUEST_FLATS_STATUS',
			status:true
		});

		if(fields.title && fields.template && fields.template != 'none'){

			try{

				let { data } = await axios.post('/admin/apartments/create-with-points', {
					...fields
				});

				dispatch({
					type:'REQUEST_FLATS_STATUS',
					status:false
				});

				await dispatch(apiblocks.getFloorByBlock(fields.block_id, fields.apartment_complex_id, fields.floor));

				NotificationManager.success('Квартира успешно создана');
				return

			}catch(err){
				dispatch({
					type:'REQUEST_FLATS_STATUS',
					status:false
				});

				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return {
					status:false
				}
			}

			return

		}

		NotificationManager.error('Заполните все поля!');
		return

	},
	getFlatsForBlock: (id, floor) => async dispatch => {

		try{

			let { data } = await axios.get(`/admin/apartments/block/${id}/${floor}`);
			console.log(223112, data)
			dispatch({
				type:'UPDATE_FLATS_FOR_BLOCK',
				data
			});

		}catch(err){
			NotificationManager.error('ошибка при загрузке квартир. Попробуйте позже');
			return
		}

	},
	updatePointsPlanSVG: (x, y, id, floor) => async dispatch => {

		try{

			let { data } = await axios.post(`/admin/apartments/points/${id}/${floor}`, {
				x,
				y
			});

			NotificationManager.success('Информация обновлена');
			return

		}catch(err){
			NotificationManager.error('ошибка при загрузке квартир. Попробуйте позже');
			return
		}

	},
	delete: (id, fields) => async dispatch => {

		let deleteStatus = window.confirm('Вы действительно хотите удалить квартиру?');

		if(deleteStatus){

			dispatch({
				type:'REQUEST_FLATS_STATUS',
				status:true
			});

			try{

				let { data } = await axios.post('/admin/apartments/delete', {
					id
				});
				
				await dispatch(flats.get());

				dispatch({
					type:'REQUEST_FLATS_STATUS',
					status:false
				});

				NotificationManager.success('Квартира успешно удалена');
				return

			}catch(err){

				console.log(333, err)

				dispatch({
					type:'REQUEST_FLATS_STATUS',
					status:false
				});

				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return
			}	

		}

	},
	getflat: (id) => async dispatch => {

		try {
			let { data } = await axios.get(`/admin/apartments/${id}`);
			console.log(222, data)
			return {
				status:true,
				...data
			}

		}catch(err){

			return {
				status:false
			}

		}

	},
	getTemplate: (id) => async dispatch => {

		try {
			let { data } = await axios.get(`/admin/apartments/templates/${id}`);

			return {
				status:true,
				...data
			}

		}catch(err){

			return {
				status:false
			}

		}

	},
	update: (fields) => async dispatch => {

		let {
			title,
			square_metres,
			count_rooms,
			material_price,
			total_price,
			square_metre_price,
			floor,
			files,
			plan_photo,
			photos,
			block_id,
			ceiling_height,
			preview_photo
		} = fields;

		if(title && preview_photo && ceiling_height && square_metres && count_rooms && material_price && total_price && square_metre_price && floor && files && plan_photo && photos){

			dispatch({
				type:'REQUEST_FLATS_STATUS',
				status:true
			});

			try{

				let { data } = await axios.post('/admin/apartments/update', {
					...fields,
					plan_photo:plan_photo.id,
					preview_photo:preview_photo.id
				});

				dispatch({
					type:'REQUEST_FLATS_STATUS',
					status:false
				});

				NotificationManager.success('Информация успешно обновлена');
				return

			}catch(err){
				dispatch({
					type:'REQUEST_FLATS_STATUS',
					status:false
				});

				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return
			}

		}

		NotificationManager.error('Заполните все поля!');
		return

	},
	updateTemplate: (fields) => async dispatch => {

		let {
			title,
			square_metres,
			count_rooms,
			material_price,
			total_price,
			square_metre_price,
			files,
			plan_photo,
			block_id,
			ceiling_height,
			preview_photo,
			apartmentComplex
		} = fields;

		if(apartmentComplex && apartmentComplex != 'none' && title && preview_photo && ceiling_height && square_metres && count_rooms && material_price && total_price && square_metre_price && files && plan_photo){

			dispatch({
				type:'REQUEST_FLATS_STATUS',
				status:true
			});

			try{

				let { data } = await axios.post('/admin/apartments/update/template', {
					...fields,
					plan_photo:plan_photo.id,
					preview_photo:preview_photo.id
				});

				dispatch({
					type:'REQUEST_FLATS_STATUS',
					status:false
				});

				NotificationManager.success('Информация успешно обновлена');
				return window.location.href = '/#/apartments/flats/templates';

			}catch(err){
				dispatch({
					type:'REQUEST_FLATS_STATUS',
					status:false
				});

				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return
			}

		}

		NotificationManager.error('Заполните все поля!');
		return

	}
}

export default flats;