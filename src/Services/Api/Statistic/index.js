import axios, { updateAxiosConfig } from '../axios';
import { SessionToken } from '../../utils/session';
import authUser from "./index";
import { NotificationManager } from 'react-notifications';
import config from '../../../Config';

const statistic = {
	get: () => async (dispatch) => {

		dispatch({
			type:'STATUS_REQUEST_STATISTIC',
			status:true
		});

		try{

			let { data } = await axios.get('/admin/statistic');

			dispatch({
				type:'UPDATE_STATISTIC_INFORMATION',
				data
			});

			dispatch({
				type:'STATUS_REQUEST_STATISTIC',
				status:false
			});

			return

		}catch(err){

			dispatch({
				type:'STATUS_REQUEST_STATISTIC',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}

	},
	settings: () => async dispatch => {

		dispatch({
			type:'STATUS_REQUEST_SETTINGS',
			status:true
		});

		try{

			let { data } = await axios.get('/admin/statistic/settings');

			dispatch({
				type:'UPDATE_STATISTIC_SETTINGS',
				data
			});

			dispatch({
				type:'STATUS_REQUEST_SETTINGS',
				status:false
			});

			return

		}catch(err){

			dispatch({
				type:'STATUS_REQUEST_SETTINGS',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}	

	},
	saveSettingPrices: (fields) => async dispatch => {

		let {
			standart,
			business,
			comfort,
			premium
		} = fields;

		if(standart && business && comfort && premium){

			dispatch({
				type:'STATUS_REQUEST_SETTINGS',
				status:true
			});

			try {

				let { data } = await axios.post('/admin/statistic/settings/prices', {
					fields
				});

				dispatch({
					type:'UPDATE_STATISTIC_SETTINGS',
					data
				});

				dispatch({
					type:'STATUS_REQUEST_SETTINGS',
					status:false
				});

				NotificationManager.success('Информация обновлена');
				return

			} catch(err) {

				dispatch({
					type:'STATUS_REQUEST_SETTINGS',
					status:false
				});

				NotificationManager.error('Ошибка, попробуйте позже');
				return
			}
		}

		NotificationManager.error('Заполните все поля');
		return

	}
}

export default statistic;