import axios, { updateAxiosConfig } from '../axios';
import { SessionToken } from '../../utils/session';
import authUser from "./index";
import { NotificationManager } from 'react-notifications';
import config from '../../../Config';

const statistic = {
	get: () => async (dispatch) => {

		dispatch({
			type:'REQUEST_STATUS_USERS',
			status:true
		});

		try{

			let { data } = await axios.get('/admin/users');

			dispatch({
				type:'UPDATE_USERS',
				data
			});

			dispatch({
				type:'REQUEST_STATUS_USERS',
				status:false
			});

			return

		}catch(err){

			dispatch({
				type:'REQUEST_STATUS_USERS',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}

	}
}

export default statistic;