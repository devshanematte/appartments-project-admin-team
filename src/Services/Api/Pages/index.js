import axios, { updateAxiosConfig } from '../axios';
import { SessionToken } from '../../utils/session';
import authUser from "./index";
import { NotificationManager } from 'react-notifications';
import config from '../../../Config';

const pages = {
	get: () => async (dispatch) => {

		dispatch({
			type:'REQUEST_STATUS_PAGES',
			status:true
		});

		try{

			let { data } = await axios.get('/admin/pages');

			dispatch({
				type:'UPDATE_PAGES',
				data
			});

			dispatch({
				type:'REQUEST_STATUS_PAGES',
				status:false
			});

			return

		}catch(err){

			dispatch({
				type:'REQUEST_STATUS_PAGES',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}

	},
	getById: (id) => async dispatch => {

		dispatch({
			type:'UPDATE_PAGE_BY_ID',
			data:null
		});

		dispatch({
			type:'REQUEST_STATUS_PAGES',
			status:true
		});

		try{

			let { data } = await axios.get(`/admin/pages/get-by-id/${id}`);

			dispatch({
				type:'UPDATE_PAGE_BY_ID',
				data
			});

			dispatch({
				type:'REQUEST_STATUS_PAGES',
				status:false
			});

			return

		}catch(err){

			dispatch({
				type:'REQUEST_STATUS_PAGES',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}	

	},
	create: (fields, history) => async dispatch => {

		let {
			builder,
			title,
			metaTitle,
			metaDesc,
			metaKeyWords,
			content
		} = fields;

		if(title && metaTitle && metaDesc && metaDesc && metaKeyWords && content){

			try {

				let { data } = await axios.post('/admin/pages/create', fields);

				NotificationManager.success(data);
				return history.goBack();

			}catch(err){
				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return
			}

		}

		NotificationManager.error('Заполните поля');
		return

	},
	update: (fields, history, id) => async dispatch => {

		let {
			builder,
			title,
			metaTitle,
			metaDesc,
			metaKeyWords,
			content,
		} = fields;

		fields.id = id;

		if(title && metaTitle && metaDesc && metaDesc && metaKeyWords && content){

			try {

				let { data } = await axios.post('/admin/pages/update', fields);

				NotificationManager.success(data);
				return history.goBack();

			}catch(err){
				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return
			}

		}

		NotificationManager.error('Заполните поля');
		return

	},	
	delete: (id) => async dispatch => {

		try {

			let resRemove = window.confirm('Страница будет удалена');

			if(resRemove){
				let { data } = await axios.post('/admin/pages/remove', {
					id
				});

				NotificationManager.success(data);
				return dispatch(pages.get());

			}

			return
			

		}catch(err){
			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return
		}

	}
}

export default pages;