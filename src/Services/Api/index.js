import { updateAxiosConfig } from './axios';
import auth from './Auth';
import cities from './Cities';
import builders from './Builders';
import apartments from './Apartments';
import files from './files';
import blocks from './Blocks';
import axios from './axios';
import flats from './Flats';
import statistic from './Statistic';
import users from './Users';
import pages from './Pages';

export default {
	updateAxiosConfig,
	auth,
	cities,
	builders,
	apartments,
	files,
	blocks,
	axios,
	flats,
	statistic,
	users,
	pages
}