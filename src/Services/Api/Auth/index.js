import axios, { updateAxiosConfig } from '../axios';
import { SessionToken } from '../../utils/session';
import authUser from "./index";
import { NotificationManager } from 'react-notifications';

const auth = {
	login: (phoneOrEmail, password) => async dispatch => {

		if(phoneOrEmail && password){

			dispatch({
				type:'REQUEST_STATUS_LOGIN',
				status:true
			});

			try{

				let { data } = await axios.post('/authentication', {
					admin:true,
					email:phoneOrEmail,
					password
				});

				if(data.type){
					NotificationManager.error('Пользователь не является админом');
					return
				}

				dispatch({
					type:'REQUEST_STATUS_LOGIN',
					status:false
				});

				updateAxiosConfig(data.token);
				SessionToken.set(data.token);

				dispatch({
					type:'UPDATE_USER',
					user:data.user
				})

				dispatch({
					type:'UPDATE_USER_LOGGEDIN',
					logedIn:true
				})
				
				return 

			}catch(err){

				dispatch({
					type:'REQUEST_STATUS_LOGIN',
					status:false
				});

				if(err.response && err.response.status && err.response.status == 400){
					NotificationManager.error(err.response.data);
					return
				}
				NotificationManager.error('Произошла ошибка. Попробуйте позже');
				return
			}

		}

		NotificationManager.warning('Заполните все поля');
		return

	},
	getProfile: () => async (dispatch) => {



	},
	logout: () => async dispatch => {
		
		SessionToken.remove();
		localStorage.removeItem('persist:root');

		updateAxiosConfig('');

		dispatch({
			type:'UPDATE_USER_LOGGEDIN',
			logedIn:false
		});

		dispatch({
			type:'UPDATE_USER',
			user:null,
			logedIn:false
		});

		return

	}
}

export default auth;