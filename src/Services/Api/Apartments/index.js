import axios, { updateAxiosConfig } from '../axios';
import { SessionToken } from '../../utils/session';
import authUser from "./index";
import { NotificationManager } from 'react-notifications';
import config from '../../../Config';

const apartments = {
	getApartmentsComplex: () => async (dispatch) => {

		dispatch({
			type:'REQUEST_APARTMENTS_STATUS',
			status:true
		});

		try{

			let { data } = await axios.get('/admin/apartments-complex');

			dispatch({
				type:'UPDATE_APARTMENTS',
				data:data.length ? data : null
			});

			setTimeout(()=>{
				dispatch({
					type:'REQUEST_APARTMENTS_STATUS',
					status:false
				});
			}, 1000)

			return

		}catch(err){

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}

	},
	editBlockApartment: (title, floors_count, id) => async dispatch => {

		if(title && floors_count){

			try {

				let { data } = await axios.post(`/admin/apartments/block/update-information`, {
					title,
					floors_count,
					id
				});

				NotificationManager.success(data);
				return

			}catch(err){
				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return
			}

		}

		return NotificationManager.error('Заполните поля');

	},
	saveEditBlockareaPoint: (idBlockEdit, editAreaPointBlock, editAreaPointBlockSTR) => async dispatch => {

		try{

			let { data } = await axios.post(`/admin/apartments/block/update-area-points`, {
				id:idBlockEdit,
				areaPoints:editAreaPointBlock,
				areaPointString:editAreaPointBlockSTR
			});

			NotificationManager.success(data);
			return	

		}catch(err){
			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return
		}

	},
	saveDotForPlan: (photo, x, y) => async dispatch => {

		try {

			let { data } = await axios.post(`/admin/apartments/360/photo/save-dot-fo-plan`, {
				id:photo._id,
				y,
				x
			});

			NotificationManager.success(data);
			return

		}catch(err){

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}

	},
	addApartment: (fields, history) => async dispatch => {

		let {
			city,
			builder,
			coords,
			title,
			address,
			photo,
			housing_class,
			image_complex_with_all_blocks,
			photos,
			min_price_square_metres
		} = fields;

		if(min_price_square_metres && city && photos && housing_class && builder && coords && title && address && photo && image_complex_with_all_blocks){

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:true
			});

			try{

				let { data } = await axios.post('/admin/apartments-complex', fields);

				dispatch({
					type:'REQUEST_APARTMENTS_STATUS',
					status:false
				});

				dispatch({
					type:'PHOTO_APARTMENT_COMPLEX',
					photo:null
				});

				return history.push('/apartments');

			}catch(err){

				dispatch({
					type:'REQUEST_APARTMENTS_STATUS',
					status:false
				});

				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return

			}

		}else{
			return NotificationManager.warning('Заполните необходимые поля');
		}

	},
	editApartment: (fields, history) => async dispatch => {

		let {
			city,
			builder,
			coords,
			title,
			address,
			photo,
			housing_class,
			min_price_square_metres
		} = fields;

		if(min_price_square_metres && city && housing_class && builder && coords && title && address && photo){

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:true
			});

			try{

				let { data } = await axios.post(`/admin/apartments-complex/edit`, fields);

				dispatch({
					type:'REQUEST_APARTMENTS_STATUS',
					status:false
				});

				dispatch({
					type:'PHOTO_APARTMENT_COMPLEX',
					photo:null
				});

				return history.push('/apartments');

			}catch(err){

				dispatch({
					type:'REQUEST_APARTMENTS_STATUS',
					status:false
				});

				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return

			}

		}else{
			return NotificationManager.warning('Заполните необходимые поля');
		}

	},
	deleteApartment: (id) => async dispatch => {

		dispatch({
			type:'REQUEST_APARTMENTS_STATUS',
			status:true
		});

		try{

			let { data } = await axios.delete(`/admin/apartments-complex/${id}`);

			NotificationManager.success('ЖК успешно удалено');

			dispatch({
				type:'UPDATE_APARTMENTS',
				data:data.length ? data : null
			});

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:false
			});

			return

		}catch(err){

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}

	},
	getApartmentById: (id, create_block = false) => async dispatch => {

		dispatch({
			type:'REQUEST_APARTMENTS_STATUS',
			status:true
		});

		try{

			let { data } = await axios.get(`/admin/apartments-complex/${id}`);

			dispatch({
				type:'UPDATE_APARTMENT_FOR_EDIT',
				data:data ? data : null
			});

			dispatch({
				type:'PHOTO_APARTMENT_COMPLEX',
				photo:{
					full_url: `${config.public}${data.photo.url}`,
					id: data.photo._id,
					url: data.photo.url
				}
			});

			if(data.image_complex_with_all_blocks){
				dispatch({
					type:'PHOTO_COMPLEX_WITH_BLOCKS',
					photo:{
						full_url: `${config.public}${data.image_complex_with_all_blocks.url}`,
						id: data.image_complex_with_all_blocks._id,
						url: data.image_complex_with_all_blocks.url
					}
				});
			} else {

				dispatch({
					type:'PHOTO_COMPLEX_WITH_BLOCKS',
					photo:null
				});

			}

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:false
			});

			return

		}catch(err){

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}

	},
	get360ApartmentById: (id) => async dispatch => {

		dispatch({
			type:'CLEAR_TEMPLATE_FOR_360'
		});

		dispatch({
			type:'UPDATE_APARTMENTS_360',
			data:null
		});

		dispatch({
			type:'REQUEST_APARTMENTS_STATUS',
			status:true
		});

		try{

			let { data } = await axios.get(`/admin/apartments/360/${id}`);

			dispatch({
				type:'UPDATE_APARTMENTS_360',
				data:data && data.data ? data.data : null
			});

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:false
			});

			dispatch({
				type:'UPDATE_TEMPLATE_FOR_360',
				template:data && data.template ? data.template : null
			});

			return

		}catch(err){

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}

	},
	addPhotos360: (photo_360, photo_360_preview, id) => async (dispatch) => {

		dispatch({
			type:'REQUEST_APARTMENTS_STATUS',
			status:true
		});

		try{

			let { data } = await axios.post(`/admin/apartments/360/add`, {
				photo_360,
				photo_360_preview,
				id
			});

			dispatch({
				type:'UPDATE_APARTMENTS_360',
				data
			});

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:false
			});

			//clear files
			dispatch({
				type:'PHOTO_APARTMENT_360',
				photo:null
			});
			dispatch({
				type:'PHOTO_APARTMENT_360_PREVIEW',
				photo:null
			});
			//end clear files

			dispatch({
				type:'UPDATE_APARTMENTS_360',
				data
			});

		}catch(err){

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:false
			});

		}

	},
	remove360Photo: (id, apartmentId) => async (dispatch) => {

		let resRemove360 = window.confirm('Фотография будет удалена?');

		if(resRemove360){


			try{

				let { data } = await axios.post('/admin/apartments/360/remove', {
					id,
					apartmentId
				});

				dispatch({
					type:'UPDATE_APARTMENTS_360',
					data
				});

				dispatch({
					type:'REQUEST_APARTMENTS_STATUS',
					status:false
				});

				return

			}catch(err){
				dispatch({
					type:'REQUEST_APARTMENTS_STATUS',
					status:false
				});
			}

			return

		}

		return

	},
	addPoint: (data_vr, id, apartmentId) => async dispatch => {

		dispatch({
			type:'REQUEST_APARTMENTS_STATUS',
			status:true
		});

		try{

			let { data } = await axios.post('/admin/apartments/360/add-point', {
				data_vr,
				id,
				apartmentId
			});

			dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:false
			});

			dispatch({
				type:'UPDATE_APARTMENTS_360',
				data
			});

			return

		}catch(err){
			return dispatch({
				type:'REQUEST_APARTMENTS_STATUS',
				status:false
			});
		}

	}
}

export default apartments;