import axios, { updateAxiosConfig } from '../axios';
import { SessionToken } from '../../utils/session';
import authUser from "./index";
import { NotificationManager } from 'react-notifications';

const auth = {
	getBuilders: () => async (dispatch) => {

		dispatch({
			type:'REQUEST_BUILDER_STATUS',
			status:true
		});

		try{

			let { data } = await axios.get('/admin/builders');

			dispatch({
				type:'UPDATE_BUILDERS',
				data:data.length ? data : null
			});

			setTimeout(()=>{
				dispatch({
					type:'REQUEST_BUILDER_STATUS',
					status:false
				});
			}, 1000)

			return

		}catch(err){

			dispatch({
				type:'REQUEST_BUILDER_STATUS',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}

	},
	addBuilder: (title, description, history, phone, year, idLogo) => async dispatch => {

		if(title && year && idLogo && idLogo.id){

			dispatch({
				type:'REQUEST_BUILDER_STATUS',
				status:true
			});	

			try{

				let { data } = await axios.post('/admin/builders', {
					title,
					description,
					phone,
					year,
					idLogo:idLogo.id
				});

				dispatch({
					type:'REQUEST_BUILDER_STATUS',
					status:false
				});

				return history.goBack();

			}catch(err){

				dispatch({
					type:'REQUEST_BUILDER_STATUS',
					status:false
				});

				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return

			}

		}

		return NotificationManager.warning('Заполните обязательные поля');

	},
	changeBuilder: (title, id, index, phone, desc, year, logoId) => async dispatch => {

		if(title && phone && logoId && year){

			try{

				let { data } = await axios.post('/admin/builders/update', {
					title,
					id,
					phone,
					logoId,
					year,
					desc
				});

				dispatch({
					type:'CHANGE_BUILDER',
					index,
					title,
					phone
				});

				return NotificationManager.success(data);

			}catch(err){

				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return

			}	

		}

		return NotificationManager.warning('Заполните обязательные поля');

	},
	removeBuilder: (id) => async dispatch => {

		try{

			let { data } = await axios.delete(`/admin/builders/${id}`);

			dispatch({
				type:'UPDATE_BUILDERS',
				data:data.length ? data : null
			});

			return NotificationManager.success('Застройщик успешно удален');

		}catch(err){

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}	

	}
}

export default auth;