import axios, { updateAxiosConfig } from '../axios';
import { SessionToken } from '../../utils/session';
import authUser from "./index";
import { NotificationManager } from 'react-notifications';

const auth = {
	getCities: () => async (dispatch) => {

		dispatch({
			type:'REQUEST_CITY_STATUS',
			status:true
		});

		try{

			let { data } = await axios.get('/admin/cities');

			dispatch({
				type:'UPDATE_CITIES',
				data:data.length ? data : null
			});

			setTimeout(()=>{
				dispatch({
					type:'REQUEST_CITY_STATUS',
					status:false
				});
			}, 1000)

			return

		}catch(err){

			dispatch({
				type:'REQUEST_CITY_STATUS',
				status:false
			});

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}

	},
	addCity: (city, description, history) => async dispatch => {

		if(city){

			dispatch({
				type:'REQUEST_CITY_STATUS',
				status:true
			});	

			try{

				let { data } = await axios.post('/admin/cities', {
					city,
					description
				});

				dispatch({
					type:'REQUEST_CITY_STATUS',
					status:false
				});

				return history.goBack();

			}catch(err){

				dispatch({
					type:'REQUEST_CITY_STATUS',
					status:false
				});

				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return

			}

		}

		return NotificationManager.warning('Заполните обязательные поля');

	},
	changeCity: (title, id, index) => async dispatch => {

		if(title){

			try{

				let { data } = await axios.post('/admin/cities/update', {
					title,
					id
				});

				dispatch({
					type:'CHANGE_CITY',
					index,
					title
				});

				return NotificationManager.success('Название города успешно изменено');

			}catch(err){

				NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
				return

			}	

		}

		return NotificationManager.warning('Заполните обязательные поля');

	},
	removeCity: (id) => async dispatch => {

		try{

			let { data } = await axios.delete(`/admin/cities/${id}`);

			dispatch({
				type:'UPDATE_CITIES',
				data:data.length ? data : null
			});

			return NotificationManager.success('Город успешно удален');

		}catch(err){

			NotificationManager.error('Упс, ошибка. Проверьте Ваши права администратора');
			return

		}	

	}
}

export default auth;