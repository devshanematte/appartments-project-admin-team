import axios, { updateAxiosConfig } from '../axios';
import { SessionToken } from '../../utils/session';
import authUser from "./index";
import { NotificationManager } from 'react-notifications';
import FormData from 'form-data';

const files = {
	uploadFile: (file, type, id = null) => async (dispatch) => {

		dispatch({
			type:'REQUEST_UPLOAD_FILE',
			status:true
		});

		let formData = new FormData();
		formData.append('uploadType', type);
		formData.append('file', file);
		formData.append('id', id);

		try{

			let { data } = await axios({
				url:'/admin/files',
				method:'post',
				data:formData,
		    	headers: {
		    		'Content-Type': 'multipart/form-data'
		    	}
		    });

			if(type == 'complex-image'){
				dispatch({
					type:'PHOTO_APARTMENT_COMPLEX',
					photo:data
				});
			}else if(type == 'images-complex-with-all-blocks'){
				dispatch({
					type:'PHOTO_COMPLEX_WITH_BLOCKS',
					photo:data
				});	
			}else if(type == 'block-image'){
				dispatch({
					type:'PHOTO_BLOCK',
					photo:data
				});	
			}else if(type == 'svg-image'){
				dispatch({
					type:'PLAN_FLAT',
					photo:data
				});	
			}else if(type == 'IMAGE'){
				dispatch({
					type:'APARTMENT_INTERIOR',
					photo:data
				});	
			}else if(type == 'IMAGE_PREVIEW'){
				dispatch({
					type:'APARTMENT_PREVIEW_PHOTO',
					photo:data
				});	
			}else if(type == '360'){
				dispatch({
					type:'PHOTO_APARTMENT_360',
					photo:data
				});	
			}else if(type == '360_PREVIEW'){
				dispatch({
					type:'PHOTO_APARTMENT_360_PREVIEW',
					photo:data
				});	
			}else if(type == 'plan-floor'){
				dispatch({
					type:'PHOTO_PLAN_FLOOR',
					photo:data
				});	
			}else if(type == 'IMAGE_FOR_POSTS'){
				
				return data;

			}else if(type == 'LOGO_BUILDER'){
				
				dispatch({
					type:'LOGO_PREVIEW_BUILDER',
					photo:data
				});	

			}

			dispatch({
				type:'REQUEST_UPLOAD_FILE',
				status:false
			});

			return

		}catch(e){

			dispatch({
				type:'REQUEST_UPLOAD_FILE',
				status:false
			});

			if(e.response.status == 400){
				NotificationManager.warning(e.response.data);
			}else{
				NotificationManager.warning('Ошибка при загрузке файла. Попробуйте позже');
			}

			return
		}

	},
	uploadFile360: (file, type) => async (dispatch) => {

		//360
		//360_PREVIEW

		dispatch({
			type:'REQUEST_UPLOAD_FILE',
			status:true
		});

		let formData = new FormData();
		formData.append('uploadType', type);
		formData.append('file', file);

		try{

			let { data } = await axios({
				url:'/admin/files',
				method:'post',
				data:formData,
		    	headers: {
		    		'Content-Type': 'multipart/form-data'
		    	}
		    });

			if(type == 'complex-image'){
				dispatch({
					type:'PHOTO_APARTMENT_COMPLEX',
					photo:data
				});
			}else if(type == 'block-image'){
				dispatch({
					type:'PHOTO_BLOCK',
					photo:data
				});	
			}else if(type == 'svg-image'){
				dispatch({
					type:'PLAN_FLAT',
					photo:data
				});	
			}else if(type == 'IMAGE'){
				dispatch({
					type:'APARTMENT_INTERIOR',
					photo:data
				});	
			}

			dispatch({
				type:'REQUEST_UPLOAD_FILE',
				status:false
			});

			return

		}catch(e){

			dispatch({
				type:'REQUEST_UPLOAD_FILE',
				status:false
			});

			if(e.response.status == 400){
				NotificationManager.warning(e.response.data);
			}else{
				NotificationManager.warning('Ошибка при загрузке файла. Попробуйте позже');
			}

			return
		}

	},
	uploadFileFlat: (file, price) => async (dispatch) => {

		dispatch({
			type:'REQUEST_UPLOAD_FILE',
			status:true
		});

		let formData = new FormData();
		formData.append('uploadType', file.type);
		formData.append('price', price);
		formData.append('paid', file.paid);
		formData.append('file', file.file);

		try{

			let { data } = await axios({
				url:'/admin/files',
				method:'post',
				data:formData,
		    	headers: {
		    		'Content-Type': 'multipart/form-data'
		    	}
		    });

			if(file.type == 'MATERIAL_BUDGET_EXCEL'){
				dispatch({
					type:'MATERIAL_BUDGET_EXCEL',
					id:data.id
				});
			}else if(file.type == 'MATERIAL_BUDGET_PDF'){
				dispatch({
					type:'MATERIAL_BUDGET_PDF',
					id:data.id
				});
			}else if(file.type == 'FLAT_ARRANGEMENT_AUTOCAD'){
				dispatch({
					type:'FLAT_ARRANGEMENT_AUTOCAD',
					id:data.id
				});
			}else if(file.type == 'APARTMENT_MODEL_3DMAX'){
				dispatch({
					type:'APARTMENT_MODEL_3DMAX',
					id:data.id
				});
			}else if(file.type == 'APARTMENT_DESIGN_REVIT'){
				dispatch({
					type:'APARTMENT_DESIGN_REVIT',
					id:data.id
				});
			}else if(file.type == 'APARTMENT_DESIGN_PDF'){
				dispatch({
					type:'APARTMENT_DESIGN_PDF',
					id:data.id
				});
			}

			dispatch({
				type:'REQUEST_UPLOAD_FILE',
				status:false
			});

			return

		}catch(e){

			dispatch({
				type:'REQUEST_UPLOAD_FILE',
				status:false
			});

			if(e.response.status == 400){
				NotificationManager.warning(e.response.data);
			}else{
				NotificationManager.warning('Ошибка при загрузке файла. Попробуйте позже');
			}

			return
		}

	}	
}

export default files;