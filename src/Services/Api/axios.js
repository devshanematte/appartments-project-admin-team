import axios from 'axios';
import config from '../../Config';
import { SessionToken } from '../utils/session';

const updateAxiosConfig = async (token) => {

	axios.defaults.baseURL = `${config.api}`;

	axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
	axios.defaults.headers.common['Access-Control-Allow-Methods'] = 'GET,PUT,POST,DELETE';

	axios.defaults.headers.common['Authorization'] = token != null ? token : '';

}

updateAxiosConfig(SessionToken.get());

export {
	updateAxiosConfig
}

export default axios;