const initialState = {
	cities:null,

	request_city_status:false
}

const cities = (state = initialState, action) => {
	switch(action.type){

		case 'UPDATE_CITIES' :
			return {
				...state,
				cities:action.data
			}

		case 'CHANGE_CITY' :



			return {
				...state,
				cities:state.cities.map((item, index)=>{

					if(index == action.index){
						item.title = action.title;
					}

					return item;

				})
			}

		case 'REQUEST_CITY_STATUS' :
			return {
				...state,
				request_city_status:action.status
			}

		default :
			return state
	}
}

export default cities;