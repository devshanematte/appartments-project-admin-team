const initialState = {
	builders:null,

	request_builder_status:false
}

const builders = (state = initialState, action) => {
	switch(action.type){

		case 'UPDATE_BUILDERS' :
			return {
				...state,
				builders:action.data
			}

		case 'CHANGE_BUILDER' :

			return {
				...state,
				builders:state.builders.map((item, index)=>{

					if(index == action.index){
						item.title = action.title;
					}

					return item;

				})
			}

		case 'REQUEST_BUILDER_STATUS' :
			return {
				...state,
				request_builder_status:action.status
			}

		default :
			return state
	}
}

export default builders;