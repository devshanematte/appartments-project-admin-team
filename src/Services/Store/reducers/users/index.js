const initialState = {
	list:null,
	
	request_status_users:false
}

const users = (state = initialState, action) => {
	switch(action.type){

		case 'UPDATE_USERS' :
			return {
				...state,
				list:action.data
			}

		case 'REQUEST_STATUS_USERS' :
			return {
				...state,
				request_status_users:action.status
			}

		default :
			return state
	}
}

export default users;