const initialState = {
	list:null,

	getFlatForEdit:null,
	blockInformation:null,

	flatsForBlock:null,

	complexApartments:null,

	request_flats_status:false,
	request_update_flat:false,

	templates:null,

	
}

const flats = (state = initialState, action) => {
	switch(action.type){

		case 'UPDATE_FLATS' :

			let updateFlatsMore = state.list == null ? action.data : [...state.list, ...action.data]

			return {
				...state,
				list:updateFlatsMore
			}

		case 'CLEAR_FLATS' :

			return {
				...state,
				list:null
			}

		case 'UPDATE_TEMPLATES' :

			let updateTemplatesMore = state.templates == null ? action.data : [...state.templates, ...action.data]

			return {
				...state,
				templates:updateTemplatesMore
			}

		case 'CLEAR_TEMPLATES' :

			return {
				...state,
				templates:null
			}

		case 'UPDATE_COMPLEX_APARTMENTS_FOR_TEMPLATES' :
			return {
				...state,
				complexApartments:action.data
			}	

		case 'UPDATE_BLOCK_FOR_FLATS' :
			return {
				...state,
				blockInformation:action.data
			}

		case 'UPDATE_POINTS_FLAT_FOR_BLOCK' :

			let updatePointsFlat = state.flatsForBlock[action.index].plan_photo_points = {
				x:action.x,
				y:action.y
			}

			return {
				...state,
				flatsForBlock:[...state.flatsForBlock]
			}

		case 'RESET_FLATS_POINTS' :
			return {
				...state,
				flatsForBlock:state.flatsForBlock.map((flat)=>{
					return {
						...flat,
						plan_photo_points:{
							x:0,
							y:0
						}
					}
				})
			}	

		case 'UPDATE_FLATS_FOR_BLOCK' :
			return {
				...state,
				flatsForBlock:action.data
			}

		case 'REQUEST_FLATS_STATUS' :
			return {
				...state,
				request_flats_status:action.status
			}

		case 'REQUEST_UPDATE_FLAT' :
			return {
				...state,
				request_update_flat:action.status
			}

		default :
			return state
	}
}

export default flats;