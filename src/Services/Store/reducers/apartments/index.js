const initialState = {
	apartments:null,

	getApartmentForEdit:null,

	request_apartments_status:false,
	request_update_blocks:false,

	template:null,

	//360
	apartment_360:null
}

const apartments = (state = initialState, action) => {
	switch(action.type){

		case 'UPDATE_TEMPLATE_FOR_360' :
			return {
				...state,
				template:action.template
			}

		case 'CLEAR_TEMPLATE_FOR_360' :
			return {
				...state,
				template:null
			}

		case 'UPDATE_APARTMENTS' :
			return {
				...state,
				apartments:action.data
			}

		case 'UPDATE_APARTMENTS_360' :
			return {
				...state,
				apartment_360:action.data
			}			

		case 'UPDATE_APARTMENT_FOR_EDIT' :
			return {
				...state,
				getApartmentForEdit:action.data
			}

		case 'RESET_APARTMENT_BLOCKS' :
			return {
				...state,
				getApartmentForEdit:{
					...state.getApartmentForEdit,
					blocks: state.getApartmentForEdit.blocks.map((block)=>{
						return {
							...block,
							photo_points:{
								x:0,
								y:0
							}
						}
					})
				}
			}

		case 'UPDATE_POINTS_BLOCK_APARTMENT_COMPLEX' :

			let updatePointsBlock = state.getApartmentForEdit.blocks[action.index].photo_points = {
				x:action.x,
				y:action.y
			}

			return {
				...state,
				getApartmentForEdit:{
					...state.getApartmentForEdit,
					blocks:state.getApartmentForEdit.blocks
				}
			}

		case 'UPDATE_BLOCKS' :

			return {
				...state,
				getApartmentForEdit:{
					...state.getApartmentForEdit,
					blocks:action.blocks
				}
			}

		case 'REQUEST_APARTMENTS_STATUS' :
			return {
				...state,
				request_apartments_status:action.status
			}

		case 'REQUEST_UPDATE_BLOCKS_FOR_APARTMENTS' :
			return {
				...state,
				request_update_blocks:action.status
			}

		default :
			return state
	}
}

export default apartments;