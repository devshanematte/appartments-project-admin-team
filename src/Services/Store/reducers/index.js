import { combineReducers } from 'redux';

import auth from './auth';
import user from './user';
import cities from './cities';
import builders from './builders';
import apartments from './apartments';
import files from './files';
import blocks from './blocks';
import flats from './flats';
import statistic from './statistic';
import users from './users';
import cache from './cache';
import pages from './pages';

export default combineReducers({
	auth,
	user,
	cities,
	builders,
	apartments,
	files,
	blocks,
	flats,
	statistic,
	users,
	cache,
	pages
})
