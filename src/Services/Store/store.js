import { applyMiddleware, compose, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import logger from 'redux-logger';
import ReduxThunk from 'redux-thunk';

export default (reducer) => {
  const persistConfig = {
    key: 'root',
    storage: storage,
    blacklist: ['files', 'apartments', 'flats', 'pages']
  }

  const middleware = [];
  const enhancers = [];

  middleware.push(logger);
  middleware.push(ReduxThunk);

  const pReducer = persistReducer(persistConfig, reducer);

  enhancers.push(applyMiddleware(...middleware));

  const store = createStore(pReducer, compose(...enhancers));
  const persistor = persistStore(store);

  return {
    store, 
    persistor
  }

}
