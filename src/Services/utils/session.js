const TOKEN_NAME = 'SESSION_TOKEN';

export const SessionToken = {
  	get: () => {
  		const token = localStorage.getItem(TOKEN_NAME);
  		return token
  	},
  	set: (token) => localStorage.setItem(TOKEN_NAME, token),
  	remove: () => localStorage.removeItem(TOKEN_NAME),
}
